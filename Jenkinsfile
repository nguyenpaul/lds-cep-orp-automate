#!/usr/bin/env groovy
@Library('sie-common-jenkins-shared-libraries') common
@Library('jenkins-shared-libraries') _

import static eu.els.ci.models.Constants.LINUX_NODE
import static eu.els.ci.common.Constants.HTTP_PROXY

pipeline {

    agent { label LINUX_NODE }

    environment {
        def CREDENTIALS = "bitbucket_compte_technique_cep"
    }

    tools {
        maven "maven-3.6.0"
        jdk 'openjdk-11'
    }

    options {
        disableConcurrentBuilds()
        buildDiscarder(logRotator(numToKeepStr: '10'))
        skipDefaultCheckout()
    }

    stages {
        stage('CleanWorkspace') {
            steps {
                cleanWs()
            }
        }

        stage('Checkout') {
            steps {
                checkout scm
            }
        }

        stage('Clean') {
            steps {
                sh "mvn clean"
            }
        }

        stage('OAS linter') {
            steps {
                withEnv(["PATH+NODEJS=${tool 'NodeJS-12.7.0'}/bin"]) {
                    sh "mvn install -pl he-oas-linter -am"
                }
            }
            post {
                always {
                    junit allowEmptyResults: true, testResults: 'he-oas-linter/spectral-junit-report.xml'
                }
            }
        }

        stage('Generate API from spec') {
            steps {
                sh "mvn install -pl he-auth-specification -am"
                sh "mvn install -pl he-admin-operations-specification -am"
                sh "mvn install -pl he-provisioning-specification -am"
                sh "mvn install -pl he-media-specification -am"
                sh "mvn install -pl he-output-specification -am"
            }
        }

        stage('Package') {
            steps {
                sh "mvn package -pl he-middle-tier -am -DskipTests"
            }
        }

        stage('Test') {
            steps {
                sh "mvn test -pl he-middle-tier -Dhttps.proxyHost=fr000-proxy002 -Dhttps.proxyPort=8080"
            }
            post {
                always {
                    junit 'he-middle-tier/target/surefire-reports/*.xml'
                }
            }
        }

        stage('SonarQube analysis') {
            steps {
                withSonarQubeEnv('sonar') {
                    sh "mvn --projects !he-oas-linter verify sonar:sonar -Dsonar.branch.name=${BRANCH_NAME}"
                }
            }
        }

        stage("Quality Gate") {
            agent none
            steps {
                timeout(time: 1, unit: 'HOURS') {
                    waitForQualityGate abortPipeline: true
                }
            }
        }

        stage('Nexus deploy') {
            when {
                expression { BRANCH_NAME ==~ /(master|develop)/ }
            }
            steps {
                sh "mvn deploy -pl he-middle-tier -am -DskipTests"
            }
        }

        stage('Gateway deploy') {
            when {
                expression { BRANCH_NAME == "param-gateway" }
            }
            steps {
                sh "rsync he-middle-tier/target/he-middle-tier-*.jar SVC-LSS-OCTOPUS@10.16.170.43:/projet/he/"
                sh "ssh SVC-LSS-OCTOPUS@10.16.170.43 'sudo systemctl stop middletier'"
                sh "ssh SVC-LSS-OCTOPUS@10.16.170.43 'sudo systemctl start middletier'"
            }
        }

        stage('Integ deploy') {
            when {
                expression { BRANCH_NAME == "develop" }
            }
            steps {
                sh "rsync he-middle-tier/target/he-middle-tier-*.jar SVC-LSS-OCTOPUS@10.16.170.42:/projet/he/"
                sh "ssh SVC-LSS-OCTOPUS@10.16.170.42 'sudo systemctl stop middletier'"
                sh "ssh SVC-LSS-OCTOPUS@10.16.170.42 'sudo systemctl start middletier'"
            }
        }
    }
}