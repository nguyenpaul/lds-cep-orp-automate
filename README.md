## Middle tier HE

Pour exécuter le projet :

- Cloner le repo en local

- Lancer : `mvn clean install`

- Lancer le middle-tier : `mvn -f he-middle-tier/pom.xml spring-boot:run`

- Exécuter le jar généré :
  `java -jar cep-lss-octo-octojournee-2\he-middle-tier\target\he-middle-tier-0.0.1-SNAPSHOT.jar`  
ou depuis le dossier he-middle-tier `mvn spring-boot:run`

Le swagger est accesible depuis l'URL : <http://localhost:9003/swagger-ui.html>

Le port exposé est renseigné dans le fichier `he-middle-tier/src/main/resources/application.properties` : `server.port=9003`

# Securité

L'authentification est réalisé par authentification LDAP
Le mot de passe de la connexion AD doit être défini dans une variable d'environnement :

## Windows

`set ldap.password=MOT_DE_PASSE`

## Linux

Les variables contenant un « . » sont illégales (sous bash, zsh) donc il est impossible de définir un mot de
passe. Il faut exécuter le profile `dev` (voir ci-dessous).

# Login / logout

## Login

Pour se logguer : <http://host:port/login>

Via requête, utilisation de FormParam :  
"username", "NOM_UTILISATEUR"  
"password", "MOT_DE_PASSE"

Ajouter à chaque requête les informations de session :  
Cookie: SessionID=example; JSESSIONID=EXAMPLE

## Logout

Pour se délogguer : <http://host:port/logout>

# Profile

Le profile `dev` permet de désactiver la partie sécurité  
`mvn spring-boot:run -Pdev`

Le profile par défaut active la sécurité avec la connexion LDAP.

# Ajout d'un modèle

## Déclaration des API

> Ajout du modèle dans : `he-provisioning-specification\src\main\resources\he-provisioning-spec.yml`

## Déclaration du modèle

> Ajout du modèle dans : `he-middle-tier\src\main\resources\he-input-spec.yml`

# Docker

Pour construire une image docker :
`docker build -t he-middle-tier he-middle-tier`

Pour lancer une image docker :
Windows :
`docker run -dp 9003:9003 he-middle-tier --spring.profiles.active=dev --marklogic.host=YOUR_IP_ADRESS`

Pour lancer une instance Marklogic, la configuration HE et le middle tier :
`docker-compose up -d`

Pour arrêter cette instance :
`docker-compose stop`

# Gatling

Le repository des tests gatling est : ./he-gatling-test

Pour lancer une simulation dev (sans authentification) :
`mvn gatling:test -Dgatling.simulationClass=eu.lefebvresarrut.he.gatling.HEDevSimulation`

Pour lancer une simulation prod (avec authentification LDAP) :
`mvn gatling:test -Dgatling.simulationClass=eu.lefebvresarrut.he.gatling.HESimulation`
