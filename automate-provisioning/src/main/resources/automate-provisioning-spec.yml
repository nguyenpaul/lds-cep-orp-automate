openapi: 3.0.3

info:
  version: 1.0.0

servers:
  - url: https://apidev.lefebvre-sarrut.eu/automate/provisioning/v1
    description: Integration Server
  - url: https://apippd.lefebvre-sarrut.eu/automate/provisioning/v1
    description: Test Server
  - url: https://api.lefebvre-sarrut.eu/automate/provisioning/v1
    description: Production Server

tags:
  - name: Provisioning
    description: Provision Automate action

paths:
  /automate/provisioning/v1/collections:
    get:
      tags:
        - Provisioning
      summary: Get all collections from the editorial hub provisioning database
      description: Get all collections from the editorial hub provisioning database
      operationId: getCollections
      parameters:
        - $ref: "../../../../he-common-specification.yml#/components/parameters/documentsSearchOptionOffsetPage"
        - $ref: "../../../../he-common-specification.yml#/components/parameters/documentsSearchOptionPageLimit"
      responses:
        "200":
          description: OK
          content:
            application/xml:
              schema:
                description: Success
                type: object
        "400":
          $ref: "../../../../he-common-specification.yml#/components/responses/BadRequest"
        "401":
          $ref: "../../../../he-common-specification.yml#/components/responses/Unauthorized"
        "403":
          $ref: "../../../../he-common-specification.yml#/components/responses/Forbidden"
        "404":
          $ref: "../../../../he-common-specification.yml#/components/responses/NotFound"
        "429":
          $ref: "../../../../he-common-specification.yml#/components/responses/TooManyRequests"
        "500":
          $ref: "../../../../he-common-specification.yml#/components/responses/InternalServerError"
        "default":
          $ref: "../../../../he-common-specification.yml#/components/responses/DefaultError"

  /automate/provisioning/v1/documents/{document-uid}:
    get:
      tags:
        - Provisioning
      description: Get a document from the Staging database
      summary: Get a document from the Staging database
      operationId: getStagingDocument
      parameters:
        - $ref: "#/components/parameters/documentPath"
        - $ref: "#/components/parameters/responseType"
      responses:
        "200":
          description: OK
          content:
            application/xml:
              schema:
                type: object
                description: Success
        "401":
          $ref: "../../../../he-common-specification.yml#/components/responses/Unauthorized"
        "403":
          $ref: "../../../../he-common-specification.yml#/components/responses/Forbidden"
        "404":
          $ref: "../../../../he-common-specification.yml#/components/responses/NotFound"
        "429":
          $ref: "../../../../he-common-specification.yml#/components/responses/TooManyRequests"
        "500":
          $ref: "../../../../he-common-specification.yml#/components/responses/InternalServerError"
        "default":
          $ref: "../../../../he-common-specification.yml#/components/responses/DefaultError"

  /automate/provisioning/v1/documents/last:
    get:
      tags:
        - Provisioning
      description: Get the list of updated document from the Staging database
      summary: Get the list of updated document from the Staging database
      operationId: searchUpdatedDocuments
      parameters:
        - $ref: "../../../../he-common-specification.yml#/components/parameters/datetimeStart"
        - $ref: "../../../../he-common-specification.yml#/components/parameters/datetimeEnd"
        - $ref: "../../../../he-common-specification.yml#/components/parameters/collections"
        - $ref: "../../../../he-common-specification.yml#/components/parameters/directoryUri"
        - $ref: "../../../../he-common-specification.yml#/components/parameters/documentsSearchOptionOffsetPage"
        - $ref: "../../../../he-common-specification.yml#/components/parameters/documentsSearchOptionPageLimit"
      responses:
        "200":
          description: OK
          content:
            application/xml:
              schema:
                description: Success
                type: object
        "401":
          $ref: "../../../../he-common-specification.yml#/components/responses/Unauthorized"
        "403":
          $ref: "../../../../he-common-specification.yml#/components/responses/Forbidden"
        "404":
          $ref: "../../../../he-common-specification.yml#/components/responses/NotFound"
        "429":
          $ref: "../../../../he-common-specification.yml#/components/responses/TooManyRequests"
        "500":
          $ref: "../../../../he-common-specification.yml#/components/responses/InternalServerError"
        "default":
          $ref: "../../../../he-common-specification.yml#/components/responses/DefaultError"

  /automate/provisioning/v1/documents:
    post:
      tags:
        - Provisioning
      description: Post a document (new or existing) into the Staging database
      summary: Post a document (new or existing) into the Staging database
      operationId: createStagingDocument
      parameters:
        - $ref: "#/components/parameters/documentId"
        - $ref: "#/components/parameters/documentType"
        - $ref: "#/components/parameters/documentState"
      requestBody:
        description: "The XML file"
        required: true
        content:
          multipart/form-data:
            schema:
              description: a description
              type: object
              properties:
                file:
                  type: string
                  description: a description
                  format: binary
      responses:
        "200":
          description: Updated
          content:
            application/xml:
              schema:
                description: Updated
                type: object
        "201":
          description: Created
          content:
            application/xml:
              schema:
                description: Created
                type: object
          headers:
            Location:
              schema:
                type: string
                example: /URL/EXIST
              description: Location
        "401":
          $ref: "../../../../he-common-specification.yml#/components/responses/Unauthorized"
        "403":
          $ref: "../../../../he-common-specification.yml#/components/responses/Forbidden"
        "404":
          $ref: "../../../../he-common-specification.yml#/components/responses/NotFound"
        "429":
          $ref: "../../../../he-common-specification.yml#/components/responses/TooManyRequests"
        "500":
          $ref: "../../../../he-common-specification.yml#/components/responses/InternalServerError"
        "default":
          $ref: "../../../../he-common-specification.yml#/components/responses/DefaultError"

  /automate/provisioning/v1/documents/{document-uid}/state:
    patch:
      tags:
        - Provisioning
      description: Update document state (NOT IMPLEMENTED)
      summary: Update document state
      operationId: setStagingDocumentState
      parameters:
        - $ref: "#/components/parameters/documentPath"
      requestBody:
        required: true
        content:
          application/json:
            schema:
              type: object
              description: The update document state request
              properties:
                document_state:
                  $ref: "#/components/schemas/DocumentState"
      responses:
        "200":
          description: Updated
          content:
            application/xml:
              schema:
                description: Updated
                type: object
        "401":
          $ref: "../../../../he-common-specification.yml#/components/responses/Unauthorized"
        "403":
          $ref: "../../../../he-common-specification.yml#/components/responses/Forbidden"
        "404":
          $ref: "../../../../he-common-specification.yml#/components/responses/NotFound"
        "429":
          $ref: "../../../../he-common-specification.yml#/components/responses/TooManyRequests"
        "500":
          $ref: "../../../../he-common-specification.yml#/components/responses/InternalServerError"
        "default":
          $ref: "../../../../he-common-specification.yml#/components/responses/DefaultError"

  /automate/provisioning/v1/documents/search:
    post:
      tags:
        - Provisioning
      description: 'Post a JSON query to search for a document in the Staging database
                    --- Example of query : {"filters":{"and":[{"type":"queryText","value":"terms to search"}]},"options":{"start":1,"pageLength":10}}'
      summary: Post a JSON query to search for a document in the Staging database
      operationId: searchStagingDocumentByJsonNode
      requestBody:
        $ref: "../../../../he-common-specification.yml#/components/requestBodies/JsonNode"
      responses:
        "200": # On devrait retourner une liste de documents paginée, dans ce cas 206 Partial Content
          description: OK
          content:
            "*/*": # JSON ?
              schema:
                $ref: "../../../../he-common-specification.yml#/components/schemas/JsonNode"
        "401":
          $ref: "../../../../he-common-specification.yml#/components/responses/Unauthorized"
        "403":
          $ref: "../../../../he-common-specification.yml#/components/responses/Forbidden"
        "404":
          $ref: "../../../../he-common-specification.yml#/components/responses/NotFound"
        "429":
          $ref: "../../../../he-common-specification.yml#/components/responses/TooManyRequests"
        "500":
          $ref: "../../../../he-common-specification.yml#/components/responses/InternalServerError"
        "default":
          $ref: "../../../../he-common-specification.yml#/components/responses/DefaultError"

  /automate/provisioning/v1/search:
    get:
      tags:
        - Provisioning
      description: Search for a document in the Staging database
      summary: Search for a document in the Staging database
      operationId: searchStagingDocumentsByString
      parameters:
        - $ref: "../../../../he-common-specification.yml#/components/parameters/documentsSearch"
        - $ref: "../../../../he-common-specification.yml#/components/parameters/documentsSearchOptionOffsetPage"
        - $ref: "../../../../he-common-specification.yml#/components/parameters/documentsSearchOptionPageLimit"
        - $ref: "../../../../he-common-specification.yml#/components/parameters/collections"
      responses:
        "200":
          description: OK
          content:
            "*/*":
              schema:
                $ref: "../../../../he-common-specification.yml#/components/schemas/JsonNode"
        "401":
          $ref: "../../../../he-common-specification.yml#/components/responses/Unauthorized"
        "403":
          $ref: "../../../../he-common-specification.yml#/components/responses/Forbidden"
        "404":
          $ref: "../../../../he-common-specification.yml#/components/responses/NotFound"
        "429":
          $ref: "../../../../he-common-specification.yml#/components/responses/TooManyRequests"
        "500":
          $ref: "../../../../he-common-specification.yml#/components/responses/InternalServerError"
        "default":
          $ref: "../../../../he-common-specification.yml#/components/responses/DefaultError"

  /automate/provisioning/v1/documents/samples:
    post:
      tags:
        - Provisioning
      description: Post a sample document into the Staging database (DEV PURPOSE ONLY)
      summary: Post a sample document into the Staging database (DEV PURPOSE ONLY)
      operationId: loadStagingSampleDocument
      parameters:
        - $ref: "#/components/parameters/documentType"
      responses:
        "201": # On pourrait retourner le payload ici (enrichi d'un identifiant auto incrémenté par exemple)
          description: Created
          content:
            application/xml:
              schema:
                description: Created
                type: object
          headers:
            Location:
              schema:
                type: string
                example: /URL/EXIST
              description: Location
        "400":
          $ref: "../../../../he-common-specification.yml#/components/responses/BadRequest"
        "401":
          $ref: "../../../../he-common-specification.yml#/components/responses/Unauthorized"
        "403":
          $ref: "../../../../he-common-specification.yml#/components/responses/Forbidden"
        "404":
          $ref: "../../../../he-common-specification.yml#/components/responses/NotFound"
        "429":
          $ref: "../../../../he-common-specification.yml#/components/responses/TooManyRequests"
        "500":
          $ref: "../../../../he-common-specification.yml#/components/responses/InternalServerError"
        "default":
          $ref: "../../../../he-common-specification.yml#/components/responses/DefaultError"

components:
  parameters:
    documentId:
      name: document_id
      in: query
      description: "ID of the document --- Example : oasis-000608"
      required: true
      schema:
        type: string
      example: oasis-000608
    documentType:
      name: type
      in: query
      description: Type of document
      example: ACTUALITE_LAQUOTIDIENNE
      required: true
      schema:
        $ref: "#/components/schemas/DocType"
    documentState:
      name: state
      in: query
      description: State of a document
      example: PUBLISHED
      schema:
        $ref: "#/components/schemas/DocumentState"
    documentPath:
      name: document-uid
      in: path
      description: "Document Path in the Staging database --- Example : /LS/FICHES/OASIS/oasis-000608.xml"
      example: /LS/FICHES/OASIS/oasis-000608.xml
      required: true
      allowReserved: true
      schema:
        type: string
    responseType:
      name: response_type
      in: query
      description: Type of response to get
      example: METADATA
      required: true
      schema:
        $ref: "#/components/schemas/ResponseType"

  schemas:
    ResponseType:
      type: string
      description: Type of response
      enum:
        - CONTENT
        - METADATA
        - ALL
    DocType:
      type: string
      description: Type of doc
      example: FORMULE
      enum:
        - ACTUALITE_IBT
        - ACTUALITE_LAQUOTIDIENNE
        - ELI
        - FORMULE
        - MARGENAE
        - FORMULENAE
        - SYNTHESENAE
        - REFERENTIELS
        - IBT$indice_taux
        - IBT$ITEMS
        - BT$editorialEntity$EFL_TEE_uiCommentaire
        - ACTUEL$article
        - ACTUDLZ$DLZ_FLASH
        - ETUDE
        - FORMULEDP$DPFORM2
        - FICHE_PRATIQUE_GB$FP
        - FICHE$OASIS
        - FICHE_FOCUS$DYNAMEM
        - REF_SIMULATEUR$ui
        - LAQUOTIDIENNE$editorialEntity$EFL_TEE_actualite
        - LAQUOTIDIENNE$editorialEntity$EFL_TEE_actualiteSimulateur
        - LAQUOTIDIENNE$editorialEntity$EFL_TEE_actualiteVideo
        - LAQUOTIDIENNE$editorialEntity$EFL_TEE_actualiteDossier
        - LAQUOTIDIENNE$editorialEntity$EFL_TEE_actualiteInfographie
        - LAQUOTIDIENNE$editorialEntity$EFL_TEE_actualitePodcast
        - LAQUOTIDIENNE$editorialEntity$EFL_TEE_actualiteTimeline
        - PAGE$editorialEntity$ELS_TEE_arbrePageGateway
        - THEME$editorialEntity$ELS_TEE_arbreThemeGateway
        - THEME$editorialEntity$ELS_TEE_arbreSousThemeGateway
        - ARBRE$editorialEntity$ELS_TEE_arbreGateway
        - VeillePermanente$editorialEntity$EL_TEE_actuActualiteVP
        - VeillePermanente$editorialEntity$EL_TEE_actuActualiteBreveVP
        - VeillePermanente$editorialEntity$EL_TEE_actuActualiteZoomVP
        - REF_CALCULETTE$item
        - TableCorrespondance$table
    DocumentState:
      type: string
      description: State of document
      example: DRAFT
      enum:
        - DRAFT
        - PUBLISHED

  responses:
    $ref: "../../../../he-common-specification.yml#/components/responses"

  securitySchemes:
    $ref: "../../../../he-common-specification.yml#/components/securitySchemes"

security:
  - oauth2: []
