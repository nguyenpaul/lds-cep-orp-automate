package lds.cep.org.automate.boot.controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import lds.cep.org.automate.api.ProvisioningApiDelegate;
import lds.cep.org.automate.boot.service.ProvisioningService;
import lds.cep.org.automate.model.DocType;
import lds.cep.org.automate.model.DocumentState;
import lds.cep.org.automate.model.ResponseType;
import lds.cep.org.automate.xml.validation.XmlValidationException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.server.ResponseStatusException;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URLDecoder;
import java.nio.charset.StandardCharsets;
import java.time.OffsetDateTime;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;
import java.util.List;

@Service
public class ProvisioningApiDelegateImpl implements ProvisioningApiDelegate {

	private static final Logger logger = LoggerFactory.getLogger(ProvisioningApiDelegateImpl.class);

	@Autowired
	private ProvisioningService provisioningService;

	@Override
	public ResponseEntity<Object> getCollections(Integer offset, Integer limit) {
		return ResponseEntity.ok().body(provisioningService.getCollections(offset, limit).get());
	}

	@Override
	public ResponseEntity<Object> createStagingDocument(String documentId, DocType type, MultipartFile file, DocumentState state) {
		logger.info("createStagingDocument ");
		try {
			byte[] doc = file.getBytes();
			provisioningService.validateXml(doc, type);
			String uriRes = provisioningService.postToHe(doc, documentId, type, getLoggerUsername(), state);
			return ResponseEntity.created(new URI(uriRes)).build();
		} catch (URISyntaxException | IOException | XmlValidationException e) {
			throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage(), e);
		}
	}

	@Override
	public ResponseEntity<Object> getStagingDocument(String documentUid, ResponseType responseType) {
		String uriDecoded = URLDecoder.decode(documentUid, StandardCharsets.UTF_8);
		switch (responseType) {
			case METADATA:
				return ResponseEntity.ok().body(provisioningService.readXMLHeMetadata(uriDecoded).toString());
			case CONTENT:
				return ResponseEntity.ok().body(provisioningService.readXMLContent(uriDecoded).toString());
			case ALL:
			default:
				return ResponseEntity.ok().body(provisioningService.readXML(uriDecoded).toString());
		}
	}

	@Override
	public ResponseEntity<Object> searchUpdatedDocuments(String datetimeStart, String datetimeEnd, List<String> collections, String directoryUri, Integer offset, Integer limit) {
		try {
			OffsetDateTime formattedDatetimeStart = OffsetDateTime.parse(datetimeStart, DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss.SSSXXX"));
			OffsetDateTime formattedDatetimeEnd = null;
			if (datetimeEnd != null) {
				formattedDatetimeEnd = OffsetDateTime.parse(datetimeEnd, DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss.SSSXXX"));
			}
			return ResponseEntity.ok().contentType(MediaType.APPLICATION_XML).body(provisioningService
					.searchUpdatedDocuments(formattedDatetimeStart, formattedDatetimeEnd, collections, directoryUri, offset, limit).get());
		}
		catch (DateTimeParseException exception) {
			throw new ResponseStatusException(HttpStatus.BAD_REQUEST, exception.getMessage(), exception);
		}
	}

	@Override
	public ResponseEntity<Object> loadStagingSampleDocument(DocType type) {
		try {
			String uriRes = provisioningService.loadSamples(type, getLoggerUsername());
			return ResponseEntity.created(new URI(uriRes)).build();
		} catch (URISyntaxException | IOException e) {
			throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage(), e);
		}
	}

	@Override
	public ResponseEntity<String> searchStagingDocumentByJsonNode(String body) {
		try {
			return ResponseEntity.ok(provisioningService.searchByJsonNode(body).toString());
		} catch (JsonProcessingException e) {
			logger.debug("Failed to parse search request");
			throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage(), e);
		}
	}

	@Override
	public ResponseEntity<String> searchStagingDocumentsByString(String q, Integer offset, Integer limit, List<String> collections) {
		try {
			return ResponseEntity.ok(provisioningService.searchByString(q, offset, limit, collections).toString());
		} catch (JsonProcessingException e) {
			logger.debug("Failed to parse search request");
			throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage(), e);
		}
	}

	private String getLoggerUsername() {
		Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
		if (!(authentication instanceof AnonymousAuthenticationToken)) {
			return authentication.getName();
		}
		return "User not found";
	}
}
