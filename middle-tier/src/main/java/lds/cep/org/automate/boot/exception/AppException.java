package lds.cep.org.automate.boot.exception;

public class AppException extends RuntimeException {


	/**
	 *
	 */
	private static final long serialVersionUID = 5387754235256869237L;

	public AppException(String message) {
		super(message);
	}

	public AppException(String message, Throwable e) {
		super(message, e);
	}

}
