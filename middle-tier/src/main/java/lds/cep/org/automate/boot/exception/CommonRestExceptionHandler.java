package lds.cep.org.automate.boot.exception;

import lds.cep.org.automate.boot.util.Utils;
import lds.cep.org.automate.model.NestedError;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.annotation.Order;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.HttpRequestMethodNotSupportedException;
import org.springframework.web.bind.MissingServletRequestParameterException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.method.annotation.MethodArgumentTypeMismatchException;
import org.springframework.web.multipart.support.MissingServletRequestPartException;
import org.springframework.web.servlet.NoHandlerFoundException;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

@ControllerAdvice
@Order
public class CommonRestExceptionHandler extends ResponseEntityExceptionHandler {

	private static final Logger logger = LoggerFactory.getLogger(CommonRestExceptionHandler.class);

	@Override
	protected ResponseEntity<Object> handleMissingServletRequestParameter(
			final MissingServletRequestParameterException ex, final HttpHeaders headers, final HttpStatus status,
			final WebRequest request) {

		final String message = ex.getParameterName() + " parameter is missing";
		var nestedError = Utils.createNestedError(ErrorConstant.MISSING_PARAMETER, ex.getParameterName(),
				message);
		final var error = Utils.createError(HttpStatus.BAD_REQUEST.value(), ErrorConstant.MISSING_PARAMETER,
				ex.getLocalizedMessage(), nestedError);

		return new ResponseEntity<>(error, new HttpHeaders(), HttpStatus.BAD_REQUEST);
	}

	@Override
	protected ResponseEntity<Object> handleHttpRequestMethodNotSupported(
			final HttpRequestMethodNotSupportedException ex, final HttpHeaders headers, final HttpStatus status,
			final WebRequest request) {

		final StringBuilder builder = new StringBuilder();
		builder.append(ex.getMethod());
		builder.append(" method is not supported for this request. Supported methods are ");
		Set<HttpMethod> method = ex.getSupportedHttpMethods();
		if (method != null) {
			method.forEach(t -> builder.append(t).append(" "));
		} else {
			builder.append("not provided.");
		}

		var nestedError = Utils.createNestedError(ErrorConstant.METHOD_NOT_SUPPORTED, ex.getMethod(),
				builder.toString());

		final var error = Utils.createError(HttpStatus.METHOD_NOT_ALLOWED.value(), ErrorConstant.METHOD_NOT_SUPPORTED,
				ex.getLocalizedMessage(), nestedError);

		return new ResponseEntity<>(error, new HttpHeaders(), HttpStatus.METHOD_NOT_ALLOWED);
	}

	@Override
	protected ResponseEntity<Object> handleMissingServletRequestPart(final MissingServletRequestPartException ex,
																	 final HttpHeaders headers, final HttpStatus status, final WebRequest request) {
		final String message = ex.getRequestPartName() + " part is missing";

		final var nestedError = Utils.createNestedError(ErrorConstant.PART_MISSING, ex.getRequestPartName(),
				message);

		final var error = Utils.createError(HttpStatus.BAD_REQUEST.value(), ErrorConstant.PART_MISSING,
				ex.getLocalizedMessage(), nestedError);
		return new ResponseEntity<>(error, new HttpHeaders(), HttpStatus.BAD_REQUEST);
	}

	//

	@ExceptionHandler({MethodArgumentTypeMismatchException.class})
	public ResponseEntity<Object> handleMethodArgumentTypeMismatch(final MethodArgumentTypeMismatchException ex,
																   final WebRequest request) {
		String message;
		Class<?> requiredType = ex.getRequiredType();
		if (requiredType != null) {
			message = ex.getName() + " should be of type " + requiredType.getName();
		} else {
			message = ex.getName() + " type mismatch";
		}

		final var nestedError = Utils.createNestedError(ErrorConstant.TYPE_MISMATCH, ex.getName(), message);

		final var error = Utils.createError(HttpStatus.BAD_REQUEST.value(), ErrorConstant.TYPE_MISMATCH,
				ex.getLocalizedMessage(), nestedError);
		return new ResponseEntity<>(error, new HttpHeaders(), HttpStatus.BAD_REQUEST);
	}

	@ExceptionHandler({ConstraintViolationException.class})
	public ResponseEntity<Object> handleConstraintViolation(final ConstraintViolationException ex,
															final WebRequest request) {

		final List<NestedError> errors = new ArrayList<>();
		for (final ConstraintViolation<?> violation : ex.getConstraintViolations()) {
			String message = violation.getRootBeanClass().getName() + " " + violation.getPropertyPath() + ": "
					+ violation.getMessage();
			final var nestedError = Utils.createNestedError(ErrorConstant.CONSTRAINT_VIOLATION,
					violation.getPropertyPath().toString(), message);
			errors.add(nestedError);
		}

		final var error = Utils.createError(HttpStatus.BAD_REQUEST.value(), ErrorConstant.CONSTRAINT_VIOLATION,
				ex.getLocalizedMessage(), errors);
		return new ResponseEntity<>(error, new HttpHeaders(), HttpStatus.BAD_REQUEST);
	}

	// 404

	@Override
	protected ResponseEntity<Object> handleNoHandlerFoundException(final NoHandlerFoundException ex,
																   final HttpHeaders headers, final HttpStatus status, final WebRequest request) {
		logger.info(ex.getClass().getName());
		//
		final String message = "No handler found for " + ex.getHttpMethod() + " " + ex.getRequestURL();

		final var nestedError = Utils.createNestedError(ErrorConstant.NO_HANDLER_FOUND, ex.getHttpMethod(),
				message);

		final var error = Utils.createError(HttpStatus.NOT_FOUND.value(), ErrorConstant.NO_HANDLER_FOUND,
				ex.getLocalizedMessage(), nestedError);
		return new ResponseEntity<>(error, new HttpHeaders(), HttpStatus.NOT_FOUND);
	}


	// 500

	@ExceptionHandler({Exception.class})
	public ResponseEntity<Object> handleAll(final Exception ex, final WebRequest request) {
		logger.error("Exception handle : {}", ex.getClass().getName());

		final var nestedError = Utils.createNestedError(ErrorConstant.INTERNAL_ERROR, ex.getClass().getName(),
				ex.getClass().toGenericString());

		final var error = Utils.createError(HttpStatus.INTERNAL_SERVER_ERROR.value(), ErrorConstant.INTERNAL_ERROR,
				ex.getMessage(), nestedError);

		return new ResponseEntity<>(error, new HttpHeaders(), HttpStatus.INTERNAL_SERVER_ERROR);
	}
}
