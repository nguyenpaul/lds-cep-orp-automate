package lds.cep.org.automate.boot.exception;

import lds.cep.org.automate.boot.util.Utils;
import lds.cep.org.automate.xml.validation.XmlValidationException;
import lds.cep.org.automate.model.Error;
import lds.cep.org.automate.model.NestedError;
import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;

@ControllerAdvice
@Order(Ordered.HIGHEST_PRECEDENCE)
public class ContentValidationExceptionHandler {

	public static final String FAILED_REQUEST_MESSAGE = "XML Validation failed";

	@ExceptionHandler(value = {XmlValidationException.class})
	protected ResponseEntity<Object> handleFailedRequest(XmlValidationException ex, WebRequest request) {
		NestedError nestedError = Utils.createNestedError(ErrorConstant.RESOURCE_VALIDATION_FAILED, "XML Document", ex.getLocalizedMessage());
		final Error error = Utils.createError(HttpStatus.BAD_REQUEST.value(), ErrorConstant.RESOURCE_FAILED_REQUEST,
				FAILED_REQUEST_MESSAGE, nestedError);

		return new ResponseEntity<>(error, new HttpHeaders(), HttpStatus.BAD_REQUEST);
	}
}
