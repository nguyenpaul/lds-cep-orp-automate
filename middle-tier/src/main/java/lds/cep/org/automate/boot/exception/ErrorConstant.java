package lds.cep.org.automate.boot.exception;

public class ErrorConstant {

	// Marklogic Error
	public static final String RESOURCE_FAILED_REQUEST = "RESOURCE_FAILED_REQUEST";
	public static final String RESOURCE_NOT_FOUND = "RESOURCE_NOT_FOUND";
	public static final String RESOURCE_ACCESS_FORBIDDEN = "RESOURCE_ACCESS_FORBIDDEN";
	public static final String UNAUTORIZED_USER = "UNAUTORIZED_USER";

	// XML Validation error
	public static final String RESOURCE_VALIDATION_FAILED = "RESOURCE_VALIDATION_FAILED";

	// HE specific Error
	public static final String JSON_PROCESSING_ERROR = "JSON_PROCESSING_ERROR";
	public static final String URI_SYNTAX_EXCEPTION = "URI_SYNTAX_EXCEPTION";
	public static final String IO_EXCEPTION = "IO_EXCEPTION";

	// Common Error
	public static final String METHOD_NOT_SUPPORTED = "METHOD_NOT_SUPPORTED";
	public static final String MISSING_PARAMETER = "MISSING_PARAMETER";
	public static final String FAILED_REQUEST = "FAILED_REQUEST";
	public static final String FORBIDDEN = "FORBIDDEN";
	public static final String ARGUMENT_NOT_VALID = "ARGUMENT_NOT_VALID";
	public static final String BIND_EXCEPTION = "BIND_EXCEPTION";
	public static final String TYPE_MISMATCH = "TYPE_MISMATCH";
	public static final String PART_MISSING = "PART_MISSING";
	public static final String CONSTRAINT_VIOLATION = "CONSTRAINT_VIOLATION";
	public static final String NO_HANDLER_FOUND = "NO_HANDLER_FOUND";
	public static final String UNSUPPORTED_MEDIA_TYPE = "UNSUPPORTED_MEDIA_TYPE";
	public static final String INTERNAL_ERROR = "INTERNAL_ERROR";

	private ErrorConstant() {
		throw new IllegalStateException("Constant class");
	}
}
