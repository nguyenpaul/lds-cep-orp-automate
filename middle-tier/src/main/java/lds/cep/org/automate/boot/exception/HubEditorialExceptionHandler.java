package lds.cep.org.automate.boot.exception;

import com.fasterxml.jackson.core.JsonProcessingException;
import lds.cep.org.automate.boot.util.Utils;
import lds.cep.org.automate.model.Error;
import lds.cep.org.automate.model.NestedError;
import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;

import java.io.IOException;
import java.net.URISyntaxException;

@ControllerAdvice
@Order(Ordered.HIGHEST_PRECEDENCE)
public class HubEditorialExceptionHandler {

	public static final String JSON_PROCESSING_ERROR_MESSAGE = "Failed to process json";
	public static final String URI_SYNTAX_ERROR_MESSAGE = "Error in syntax URI";
	private static final String IO_EXCEPTION_MESSAGE = "IO error";

	@ExceptionHandler(value = {JsonProcessingException.class})
	protected ResponseEntity<Object> handleJsonProcessingException(JsonProcessingException ex, WebRequest request) {
		lds.cep.org.automate.model.NestedError nestedError = Utils.createNestedError(ErrorConstant.JSON_PROCESSING_ERROR,
				"Json", ex.getLocalizedMessage());
		final lds.cep.org.automate.model.Error error = Utils.createError(HttpStatus.BAD_REQUEST.value(), ErrorConstant.RESOURCE_FAILED_REQUEST,
				JSON_PROCESSING_ERROR_MESSAGE, nestedError);

		return new ResponseEntity<>(error, new HttpHeaders(), HttpStatus.BAD_REQUEST);
	}

	@ExceptionHandler(value = {URISyntaxException.class})
	protected ResponseEntity<Object> handleURISyntaxException(URISyntaxException ex, WebRequest request) {
		lds.cep.org.automate.model.NestedError nestedError = Utils.createNestedError(ErrorConstant.URI_SYNTAX_EXCEPTION, "URI",
				ex.getLocalizedMessage());
		final lds.cep.org.automate.model.Error error = Utils.createError(HttpStatus.INTERNAL_SERVER_ERROR.value(),
				ErrorConstant.URI_SYNTAX_EXCEPTION, URI_SYNTAX_ERROR_MESSAGE, nestedError);

		return new ResponseEntity<>(error, new HttpHeaders(), HttpStatus.INTERNAL_SERVER_ERROR);
	}

	@ExceptionHandler(value = {IOException.class})
	protected ResponseEntity<Object> handleIOException(IOException ex, WebRequest request) {
		NestedError nestedError = Utils.createNestedError(ErrorConstant.IO_EXCEPTION, "IO", ex.getLocalizedMessage());
		final Error error = Utils.createError(HttpStatus.INTERNAL_SERVER_ERROR.value(), ErrorConstant.IO_EXCEPTION,
				IO_EXCEPTION_MESSAGE, nestedError);

		return new ResponseEntity<>(error, new HttpHeaders(), HttpStatus.INTERNAL_SERVER_ERROR);
	}

}
