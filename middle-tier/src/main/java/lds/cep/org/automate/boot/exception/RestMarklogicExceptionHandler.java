package lds.cep.org.automate.boot.exception;

import com.marklogic.client.FailedRequestException;
import com.marklogic.client.ForbiddenUserException;
import com.marklogic.client.ResourceNotFoundException;
import com.marklogic.client.UnauthorizedUserException;
import lds.cep.org.automate.boot.util.Utils;
import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;

@ControllerAdvice
@Order(Ordered.HIGHEST_PRECEDENCE)
public class RestMarklogicExceptionHandler {

	public static final String RESSOURCE_NOT_FOUND_MESSAGE = "Document URI not found";
	public static final String FORBIDDEN_MESSAGE = "Access denied : forbidden";
	public static final String FAILED_REQUEST_MESSAGE = "Failed request from database";
	public static final String UNAUTHORIZED_USER = "UNAUTHORIZED_USER";

	@ExceptionHandler(value = {FailedRequestException.class})
	protected ResponseEntity<Object> handleFailedRequest(FailedRequestException ex, WebRequest request) {
		if (ex.getMessage().contains("Unauthorized")) {
			return handleUnauthorizedUser(new UnauthorizedUserException(ex), request);
		}

		var nestedError = Utils.createNestedError(ErrorConstant.FAILED_REQUEST, "Internal error", FAILED_REQUEST_MESSAGE);
		final var error = Utils.createError(HttpStatus.BAD_REQUEST.value(), ErrorConstant.RESOURCE_FAILED_REQUEST,
				ex.getLocalizedMessage(), nestedError);

		return new ResponseEntity<>(error, new HttpHeaders(), HttpStatus.BAD_REQUEST);
	}

	@ExceptionHandler(value = {ResourceNotFoundException.class})
	protected ResponseEntity<Object> handleConflict(ResourceNotFoundException ex, WebRequest request) {
		var nestedError = Utils.createNestedError(ErrorConstant.RESOURCE_NOT_FOUND, "Document URI", RESSOURCE_NOT_FOUND_MESSAGE);
		final var error = Utils.createError(HttpStatus.NOT_FOUND.value(), ErrorConstant.RESOURCE_NOT_FOUND,
				ex.getLocalizedMessage(), nestedError);

		return new ResponseEntity<>(error, new HttpHeaders(), HttpStatus.NOT_FOUND);
	}

	@ExceptionHandler(value = {ForbiddenUserException.class})
	protected ResponseEntity<Object> handleForbidden(ForbiddenUserException ex, WebRequest request) {
		var nestedError = Utils.createNestedError(ErrorConstant.FORBIDDEN, "Authorization", FORBIDDEN_MESSAGE);
		final var error = Utils.createError(HttpStatus.FORBIDDEN.value(), ErrorConstant.RESOURCE_ACCESS_FORBIDDEN,
				ex.getLocalizedMessage(), nestedError);

		return new ResponseEntity<>(error, new HttpHeaders(), HttpStatus.FORBIDDEN);
	}

	@ExceptionHandler(value = {UnauthorizedUserException.class})
	protected ResponseEntity<Object> handleUnauthorizedUser(UnauthorizedUserException ex, WebRequest request) {
		var nestedError = Utils.createNestedError(ErrorConstant.UNAUTORIZED_USER, "Unauthorized user", UNAUTHORIZED_USER);
		final var error = Utils.createError(HttpStatus.UNAUTHORIZED.value(), ErrorConstant.UNAUTORIZED_USER,
				ex.getLocalizedMessage(), nestedError);

		return new ResponseEntity<>(error, new HttpHeaders(), HttpStatus.UNAUTHORIZED);
	}

}
