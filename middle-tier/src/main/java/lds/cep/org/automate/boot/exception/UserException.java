package lds.cep.org.automate.boot.exception;

public class UserException extends RuntimeException {


	/**
	 *
	 */
	private static final long serialVersionUID = 7496267413468993052L;

	public UserException(String message) {
		super(message);
	}

	public UserException(String message, Throwable e) {
		super(message, e);
	}

}
