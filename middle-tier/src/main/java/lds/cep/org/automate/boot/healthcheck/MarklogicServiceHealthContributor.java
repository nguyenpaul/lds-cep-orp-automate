package lds.cep.org.automate.boot.healthcheck;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.actuate.health.CompositeHealthContributor;
import org.springframework.boot.actuate.health.HealthContributor;
import org.springframework.boot.actuate.health.NamedContributor;
import org.springframework.stereotype.Component;

import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;

@Component
public class MarklogicServiceHealthContributor implements CompositeHealthContributor {

	private final Map<String, HealthContributor> contributors = new LinkedHashMap<>();

	@Autowired
	public MarklogicServiceHealthContributor(MarklogicStagingServiceHealthIndicator marklogicServiceHealthIndicator,
											 MarklogicJobsServiceHealthIndicator marklogicJobsServiceHealthIndicator,
											 MarklogicFinalServiceHealthIndicator marklogicFinalServiceHealthIndicator,
											 MarklogicMediaServiceHealthIndicator marklogicMediaServiceHealthIndicator) {
		contributors.put("MarklogicStagingService", marklogicServiceHealthIndicator);
		contributors.put("MarklogicJobsService", marklogicJobsServiceHealthIndicator);
		contributors.put("MarklogicMediaService", marklogicMediaServiceHealthIndicator);
		contributors.put("MarklogicFinalService", marklogicFinalServiceHealthIndicator);

	}

	/**
	 * return list of health contributors
	 */
	@Override
	public Iterator<NamedContributor<HealthContributor>> iterator() {
		return contributors.entrySet().stream().map(entry -> NamedContributor.of(entry.getKey(), entry.getValue()))
				.iterator();
	}

	@Override
	public HealthContributor getContributor(String name) {
		return contributors.get(name);
	}
}
