package lds.cep.org.automate.boot.healthcheck;

import com.marklogic.client.DatabaseClient;
import com.marklogic.client.DatabaseClient.ConnectionResult;
import lds.cep.org.automate.boot.ml.MarklogicConfig;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.actuate.health.Health;
import org.springframework.boot.actuate.health.HealthIndicator;
import org.springframework.stereotype.Component;

@Component
public class MarklogicStagingServiceHealthIndicator implements HealthIndicator {

	@Autowired
	MarklogicConfig mlConfig;

	@Override
	public Health health() {
		DatabaseClient client = mlConfig.getStagingClient();
		ConnectionResult result = client.checkConnection();
		if (!result.isConnected()) {
			return Health.down().withDetail("ErrorCode", result.getStatusCode())
					.withDetail("ErrorMessage", result.getErrorMessage()).build();
		}
		return Health.up().build();
	}
}
