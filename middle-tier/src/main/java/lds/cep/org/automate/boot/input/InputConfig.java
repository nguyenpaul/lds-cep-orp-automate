package lds.cep.org.automate.boot.input;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.yaml.YAMLFactory;
import lds.cep.org.automate.boot.exception.AppException;
import lds.cep.org.automate.model.DocType;
import org.springframework.context.annotation.Configuration;

import javax.annotation.PostConstruct;
import java.io.IOException;
import java.io.InputStream;
import java.util.Map;

@Configuration
public class InputConfig {

	private static final ClassLoader classLoader = InputConfig.class.getClassLoader();

	private Map<String, InputLine> inputMap;

	public Map<String, InputLine> getInputMap() {
		return inputMap;
	}

	@PostConstruct
	public void init() throws IOException {
		ObjectMapper mapper = new ObjectMapper(new YAMLFactory());
		mapper.findAndRegisterModules();

		try (InputStream inputStream = classLoader.getResourceAsStream("he-input.yml")) {
			inputMap = mapper.readValue(inputStream, new TypeReference<>() {
			});
		} catch (IllegalArgumentException e) {
			throw new AppException("Input stream not found", e);
		}
	}

	public InputLine getInputLine(DocType docType) {
		return this.inputMap.get(docType.toString());
	}
}
