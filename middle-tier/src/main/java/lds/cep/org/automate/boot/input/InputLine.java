package lds.cep.org.automate.boot.input;

import java.util.List;

public class InputLine {
	private String name;
	private String publisher;
	private String stagingUri;
	private List<String> jobsUris;
	private List<String> collections;
	private List<String> models;

	public InputLine() {
	}

	public InputLine(String name, String publisher, String stagingUri, List<String> jobsUris,
					 List<String> collections, List<String> models) {
		this.name = name;
		this.publisher = publisher;
		this.stagingUri = stagingUri;
		this.jobsUris = jobsUris;
		this.collections = collections;
		this.models = models;
	}

	public String getStagingUri() {
		return stagingUri;
	}

	public void setStagingUri(String stagingUri) {
		this.stagingUri = stagingUri;
	}

	public List<String> getJobsUris() {
		return jobsUris;
	}

	public void setJobsUris(List<String> jobsUris) {
		this.jobsUris = jobsUris;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getPublisher() {
		return publisher;
	}

	public void setPublisher(String publisher) {
		this.publisher = publisher;
	}

	public List<String> getCollections() {
		return collections;
	}

	public void setCollections(List<String> collections) {
		this.collections = collections;
	}

	public List<String> getModels() {
		return models;
	}

	public void setModels(List<String> models) {
		this.models = models;
	}
}
