package lds.cep.org.automate.boot.ml;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;

/**
 * State of document
 */
public enum DocumentOption {

	FULL("FULL"),

	METADATA("METADATA"),

	CONTENT("CONTENT");

	private final String value;

	DocumentOption(String value) {
		this.value = value;
	}

	@JsonValue
	public String getValue() {
		return value;
	}

	@Override
	public String toString() {
		return String.valueOf(value);
	}

	@JsonCreator
	public static DocumentOption fromValue(String value) {
		for (DocumentOption b : DocumentOption.values()) {
			if (b.value.equals(value)) {
				return b;
			}
		}
		throw new IllegalArgumentException("Unexpected value '" + value + "'");
	}
}

