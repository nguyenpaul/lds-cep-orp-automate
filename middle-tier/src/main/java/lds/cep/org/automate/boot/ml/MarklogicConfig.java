package lds.cep.org.automate.boot.ml;

import com.marklogic.client.DatabaseClient;
import com.marklogic.client.DatabaseClientFactory;
import com.marklogic.client.admin.ServerConfigurationManager;
import com.marklogic.client.document.BinaryDocumentManager;
import com.marklogic.client.document.GenericDocumentManager;
import com.marklogic.client.document.JSONDocumentManager;
import com.marklogic.client.document.XMLDocumentManager;
import com.marklogic.client.query.QueryManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;

@Configuration
@ConfigurationProperties(prefix = "marklogic")
public class MarklogicConfig implements InitializingBean {

	private static final Logger logger = LoggerFactory.getLogger(MarklogicConfig.class);

	public static final String NAMESPACE_HE = "http://www.lefebvre-sarrut.eu/ns/hubeditorial";

	@Value("${marklogic.Host}")
	private String host;

	@Value("${marklogic.StagingRestPort}")
	private Integer stagingRestPort;

	@Value("${marklogic.JobsRestPort}")
	private Integer jobsRestPort;

	@Value("${marklogic.FinalRestPort}")
	private Integer finalRestPort;

	@Value("${marklogic.MediaRestPort}")
	private Integer mediaRestPort;

	@Value("${marklogic.SimpleSsl}")
	private boolean simpleSsl;

	@Value("${marklogic.Username:admin}")
	private String username;

	@Value("${marklogic.Password:admin}")
	private String password;

	@Value("${marklogic.BasicAuth:true}")
	private Boolean basicAuth;

	private DatabaseClient stagingClient;
	private DatabaseClient jobsClient;
	private DatabaseClient mediaClient;
	private DatabaseClient finalClient;

	@PostConstruct
	void init() {
		if (isBasicAuth()) {
			stagingClient = DatabaseClientFactory.newClient(getHost(), getStagingRestPort(),
					new DatabaseClientFactory.BasicAuthContext(getUsername(), getPassword()));
			jobsClient = DatabaseClientFactory.newClient(getHost(), getJobsRestPort(),
					new DatabaseClientFactory.BasicAuthContext(getUsername(), getPassword()));
			mediaClient = DatabaseClientFactory.newClient(getHost(), getMediaRestPort(),
					new DatabaseClientFactory.BasicAuthContext(getUsername(), getPassword()));
			finalClient = DatabaseClientFactory.newClient(getHost(), getFinalRestPort(),
					new DatabaseClientFactory.BasicAuthContext(getUsername(), getPassword()));

		} else {
			stagingClient = DatabaseClientFactory.newClient(getHost(), getStagingRestPort(),
					new DatabaseClientFactory.DigestAuthContext(getUsername(), getPassword()));
			jobsClient = DatabaseClientFactory.newClient(getHost(), getJobsRestPort(),
					new DatabaseClientFactory.DigestAuthContext(getUsername(), getPassword()));
			mediaClient = DatabaseClientFactory.newClient(getHost(), getMediaRestPort(),
					new DatabaseClientFactory.DigestAuthContext(getUsername(), getPassword()));
			finalClient = DatabaseClientFactory.newClient(getHost(), getFinalRestPort(),
					new DatabaseClientFactory.DigestAuthContext(getUsername(), getPassword()));
		}
	}

	@PreDestroy
	void clean() {
		if (stagingClient != null) {
			stagingClient.release();
			stagingClient = null;
		}
		if (jobsClient != null) {
			jobsClient.release();
			jobsClient = null;
		}

		if (finalClient != null) {
			finalClient.release();
			finalClient = null;
		}

		if (mediaClient != null) {
			mediaClient.release();
			mediaClient = null;
		}

	}

	public DatabaseClient newDatabaseClient(String username, String password) {
		DatabaseClient client;
		if (isBasicAuth()) {
			client = DatabaseClientFactory.newClient(getHost(), getFinalRestPort(),
					new DatabaseClientFactory.BasicAuthContext(username, password));
		} else {
			client = DatabaseClientFactory.newClient(getHost(), getFinalRestPort(),
					new DatabaseClientFactory.DigestAuthContext(username, password));
		}
		return client;
	}

	public DatabaseClient getStagingClient() {
		return stagingClient;
	}

	public DatabaseClient getJobsClient() {
		return jobsClient;
	}

	public DatabaseClient getMediaClient() {
		return mediaClient;
	}

	public DatabaseClient getFinalClient() {
		return finalClient;
	}

	public DatabaseClient getDatabaseClient(Database database) {
		switch (database) {
			case STAGING:
				return getStagingClient();
			case JOBS:
				return getJobsClient();
			case MEDIA:
				return getMediaClient();
			case FINAL:
			default:
				return getFinalClient();
		}
	}

	public XMLDocumentManager newXMLDocumentManager(Database base) {
		DatabaseClient client = getDatabaseClient(base);
		return client.newXMLDocumentManager();
	}

	public QueryManager newQueryManager(Database base) {
		DatabaseClient client = getDatabaseClient(base);
		return client.newQueryManager();
	}

	public JSONDocumentManager newJSONDocumentManager(Database base) {
		DatabaseClient client = getDatabaseClient(base);
		return client.newJSONDocumentManager();
	}

	public BinaryDocumentManager newBinaryDocumentManager(Database base) {
		DatabaseClient client = getDatabaseClient(base);
		return client.newBinaryDocumentManager();
	}

	public GenericDocumentManager newGenericDocumentManager(Database base) {
		DatabaseClient client = getDatabaseClient(base);
		return client.newDocumentManager();
	}

	public ServerConfigurationManager newServerConfigManager(Database base) {
		DatabaseClient client = getDatabaseClient(base);
		return client.newServerConfigManager();
	}

	public enum Database {
		STAGING, JOBS, FINAL, MEDIA
	}

	public String getPassword() {
		return password;
	}

	@Override
	public void afterPropertiesSet() {
		if (logger.isInfoEnabled())
			logger.info(String.format("Will connect to MarkLogic at %s:%d", host, stagingRestPort));
	}

	public String getHost() {
		return host;
	}

	public Integer getJobsRestPort() {
		return jobsRestPort;
	}

	public Integer getStagingRestPort() {
		return stagingRestPort;
	}

	public Integer getMediaRestPort() {
		return mediaRestPort;
	}

	public boolean getSimpleSsl() {
		return simpleSsl;
	}

	public String getUsername() {
		return username;
	}

	public boolean isBasicAuth() {
		return basicAuth;
	}

	public int getFinalRestPort() {
		return finalRestPort;
	}

	public void setBasicAuth(Boolean basicAuth) {
		this.basicAuth = basicAuth;
	}

	public void setHost(String host) {
		this.host = host;
	}

	public void setStagingRestPort(Integer stagingRestPort) {
		this.stagingRestPort = stagingRestPort;
	}

	public void setJobsRestPort(Integer jobsRestPort) {
		this.jobsRestPort = jobsRestPort;
	}

	public void setFinalRestPort(Integer finalRestPort) {
		this.finalRestPort = finalRestPort;
	}

	public void setMediaRestPort(Integer mediaRestPort) {
		this.mediaRestPort = mediaRestPort;
	}

	public void setSimpleSsl(boolean simpleSsl) {
		this.simpleSsl = simpleSsl;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public void setStagingClient(DatabaseClient stagingClient) {
		this.stagingClient = stagingClient;
	}

	public void setJobsClient(DatabaseClient jobsClient) {
		this.jobsClient = jobsClient;
	}

	public void setMediaClient(DatabaseClient mediaClient) {
		this.mediaClient = mediaClient;
	}

	public void setFinalClient(DatabaseClient finalClient) {
		this.finalClient = finalClient;
	}
}
