package lds.cep.org.automate.boot.ml;

import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.marklogic.client.document.*;
import com.marklogic.client.document.DocumentManager.Metadata;
import com.marklogic.client.io.*;
import com.marklogic.client.query.*;
import com.marklogic.client.query.StructuredQueryBuilder.Operator;
import lds.cep.org.automate.boot.search.*;
import lds.cep.org.automate.boot.util.SearchUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.xml.namespace.QName;
import java.time.OffsetDateTime;
import java.util.ArrayList;
import java.util.List;

@Service
public class MarklogicService {

	final MarklogicConfig marklogicConfig;
	private static final Logger logger = LoggerFactory.getLogger(MarklogicService.class);

	@Autowired
	public MarklogicService(MarklogicConfig marklogicConfig) {
		super();
		this.marklogicConfig = marklogicConfig;
	}

	private ServerTransform getReadXSL(DocumentOption documentOption) {
		if (documentOption == null) {
			return null;
		}
		ServerTransform serverTransform = new ServerTransform("get-document");
		serverTransform.put("documentOption", documentOption.toString());
		return serverTransform;
	}

	public JsonNode searchByJsonNode(JsonNode searchRequest, MarklogicConfig.Database database, String searchOption) {
		long start = 1;
		long pageLength = 10;
		if (searchRequest.has("options")) {
			ObjectNode options = (ObjectNode) searchRequest.get("options");
			if (options.has("start")) {
				start = options.get("start").asLong();
			}
			if (options.has("pageLength")) {
				pageLength = options.get("pageLength").asLong();
			}
		}
		QueryManager mgr = this.marklogicConfig.newQueryManager(database);
		mgr.setPageLength(pageLength);

		SearchQueryBuilder myquery = new SearchQueryBuilder(searchOption);
		StructuredQueryDefinition query = myquery.buildQueryWithCriteria(searchRequest.get("filters"));

		JacksonHandle jHandle = mgr.search(query, new JacksonHandle(), start);
		JsonNode jNode = jHandle.get();
		logger.info(jNode.toString());
		return SearchUtil.processResults(jNode);
	}

	public JsonNode search(String searchRequest, Integer start, Integer length, MarklogicConfig.Database database, String searchOption)
			throws JsonProcessingException {
		ObjectMapper mapper = new ObjectMapper();

		JsonForSearch jsonForSearch = new JsonForSearch();
		Options options = new Options();
		Filters filters = new Filters();
		AndQuery andQuery = new AndQuery();
		List<And> and2 = new ArrayList<>();
		String jsonString;

		andQuery.setValue(searchRequest);
		and2.add(andQuery);
		filters.setAnd(and2);
		jsonForSearch.setFilters(filters);

		options.setStart(start);
		options.setPageLength(length);
		jsonForSearch.setOptions(options);

		mapper.setSerializationInclusion(Include.NON_NULL);
		jsonString = mapper.writeValueAsString(jsonForSearch);
		JsonNode jsonQuery = mapper.readTree(jsonString);
		logger.info(jsonQuery.toString());
		return searchByJsonNode(jsonQuery, database, searchOption);
	}

	public JsonNode searchByString(String searchRequest, Integer start, Integer length, MarklogicConfig.Database database, String searchOption, List<String> collections)
			throws JsonProcessingException {
		ObjectMapper mapper = new ObjectMapper();

		JsonForSearch jsonForSearch = new JsonForSearch();
		Options options = new Options();
		Filters filters = new Filters();
		AndQuery andQuery = new AndQuery();
		List<And> andList = new ArrayList<>();
		String jsonString;

		andQuery.setType("queryText");
		andQuery.setValue(searchRequest);
		andList.add(andQuery);

		if (collections != null && !collections.isEmpty()) {
			AndCollections andCollection = new AndCollections();
			andCollection.setConstraint("Collection");
			andCollection.setConstraintType("collection");
			andCollection.setMode("and");
			andCollection.setType("selection");
			andCollection.setValue(collections);
			andList.add(andCollection);
		}

		filters.setAnd(andList);
		jsonForSearch.setFilters(filters);

		options.setStart(start);
		options.setPageLength(length);
		jsonForSearch.setOptions(options);

		mapper.setSerializationInclusion(Include.NON_NULL);
		jsonString = mapper.writeValueAsString(jsonForSearch);
		JsonNode jsonQuery = mapper.readTree(jsonString);
		return searchByJsonNode(jsonQuery, database, searchOption);
	}

	public StringHandle searchCustom(MarklogicConfig.Database database, OffsetDateTime datetimeStart, OffsetDateTime datetimeEnd,
                                     List<String> collections, String directoryURI, Integer start, Integer length) {

		QueryManager mgr = this.marklogicConfig.newQueryManager(database);
		mgr.setPageLength(length);

		StructuredQueryDefinition query;
		StructuredQueryBuilder qb = mgr.newStructuredQueryBuilder("lastDocuments");

		if (directoryURI == null || directoryURI.isEmpty()) {
			directoryURI = "/";
		}

		if (datetimeEnd == null) {
			datetimeEnd = OffsetDateTime.now();
		}

		StructuredQueryDefinition criteriaDirectoryURI = qb.directory(true, directoryURI);
		StructuredQueryDefinition criteriaStartDatetime = qb.rangeConstraint("update-date", Operator.GE,
				datetimeStart.toString());
		StructuredQueryDefinition criteriaEndDatetime = qb.rangeConstraint("update-date", Operator.LE,
				datetimeEnd.toString());

		if (collections != null && !collections.isEmpty()) {
			StructuredQueryDefinition criteriaCollections = qb.collection(collections.stream().toArray(String[]::new));
			query = qb.and(criteriaStartDatetime, criteriaEndDatetime, criteriaDirectoryURI, criteriaCollections);
		} else {
			query = qb.and(criteriaStartDatetime, criteriaEndDatetime, criteriaDirectoryURI);
		}

		return mgr.search(query, new StringHandle(), start);
	}

	public StringHandle readXML(String uri, MarklogicConfig.Database database, String transform) {
		ServerTransform serverTransform = new ServerTransform(transform);
		XMLDocumentManager xmlDocMgr = marklogicConfig.newXMLDocumentManager(database);
		return xmlDocMgr.read(uri, new StringHandle(), serverTransform);
	}

	public StringHandle readXML(String uri, MarklogicConfig.Database database, DocumentOption documentOption) {
		ServerTransform serverTransform = getReadXSL(documentOption);
		XMLDocumentManager xmlDocMgr = marklogicConfig.newXMLDocumentManager(database);
		if (serverTransform != null) {
			return xmlDocMgr.read(uri, new StringHandle(), serverTransform);
		}
		return xmlDocMgr.read(uri, new StringHandle());
	}

	public void saveMultipleXML(List<String> uris, MarklogicConfig.Database database, InputStreamHandle content,
                                List<String> collections) {
		XMLDocumentManager xmlMgr = this.marklogicConfig.newXMLDocumentManager(database);

		DocumentMetadataHandle documentMetadataHandle = new DocumentMetadataHandle();
		for (String collection : collections) {
			documentMetadataHandle.getCollections().add(collection);
		}
		for (String uri : uris) {
			xmlMgr.write(uri, documentMetadataHandle, content);
		}
	}

	public void saveXML(String uri, MarklogicConfig.Database database, InputStreamHandle content, List<String> collections) {
		XMLDocumentManager xmlMgr = this.marklogicConfig.newXMLDocumentManager(database);

		DocumentMetadataHandle documentMetadataHandle = new DocumentMetadataHandle();
		for (String collection : collections) {
			documentMetadataHandle.getCollections().add(collection);
		}
		xmlMgr.write(uri, documentMetadataHandle, content);
	}

	public StringHandle readJSON(String uri, MarklogicConfig.Database database) {
		JSONDocumentManager jsonDocMgr = marklogicConfig.newJSONDocumentManager(database);
		return jsonDocMgr.read(uri, new StringHandle());
	}

	public StringHandle readProperties(String uri, MarklogicConfig.Database database, Metadata metaType) {
		XMLDocumentManager xmlDocMgr = marklogicConfig.newXMLDocumentManager(database);
		xmlDocMgr.setMetadataCategories(metaType);
		return xmlDocMgr.readMetadata(uri, new StringHandle());
	}

	public void deleteAllDocuments(String uri, MarklogicConfig.Database database) {
		QueryManager queryMgr = marklogicConfig.newQueryManager(database);
		DeleteQueryDefinition dq = queryMgr.newDeleteDefinition();
		dq.setDirectory(String.format("%s/", uri));
		queryMgr.delete(dq);
	}

	public Boolean copyAllDocuments(String uri, MarklogicConfig.Database source, MarklogicConfig.Database dest) {
		XMLDocumentManager sourceXmlDocMgr = marklogicConfig.newXMLDocumentManager(source);
		XMLDocumentManager destXmlDocMgr = marklogicConfig.newXMLDocumentManager(dest);
		QueryManager sourceQueryMgr = marklogicConfig.newQueryManager(source);
		StringQueryDefinition query = sourceQueryMgr.newStringDefinition();
		query.setDirectory(String.format("%s/", uri));

		DocumentPage documents = sourceXmlDocMgr.search(query, 1);
		if (documents.hasContent()) {
			while (documents.hasNext()) {
				DocumentRecord document = documents.next();
				destXmlDocMgr.write(document.getUri(), document.getContent(new DOMHandle()));
			}
			return true;
		} else {
			return false;
		}
	}

	public void saveBinary(String uriHeMedia, MarklogicConfig.Database database, String mimeType, InputStreamHandle content) {
		BinaryDocumentManager docBinMgr = marklogicConfig.newBinaryDocumentManager(database);
		docBinMgr.setMetadataExtraction(BinaryDocumentManager.MetadataExtraction.PROPERTIES);
		DocumentMetadataHandle metadataHandle = new DocumentMetadataHandle();
		DocumentMetadataHandle.DocumentCollections collections = metadataHandle.getCollections();
		collections.add(mimeType);
		docBinMgr.write(uriHeMedia, metadataHandle, content);
	}

	public void addProperty(String docUri, MarklogicConfig.Database database, QName qNameProperties, String valueProperties) {
		GenericDocumentManager genericDocumentManager = marklogicConfig.getDatabaseClient(database).newDocumentManager();
		DocumentMetadataPatchBuilder documentMetadataPatchBuilder = genericDocumentManager.newPatchBuilder(Format.XML);
		documentMetadataPatchBuilder.addPropertyValue(qNameProperties, valueProperties);
		genericDocumentManager.patch(docUri, documentMetadataPatchBuilder.build());
	}

	public byte[] getBinary(String uriHeMedia, MarklogicConfig.Database database) {
		BinaryDocumentManager docBinMgr = marklogicConfig.newBinaryDocumentManager(database);
		return docBinMgr.read(uriHeMedia, new BytesHandle()).get();
	}

	public byte[] getContentDocument(String uri, MarklogicConfig.Database database) {
		GenericDocumentManager docMgr = marklogicConfig.newGenericDocumentManager(database);
		return docMgr.read(uri, new BytesHandle()).get();
	}

	public JacksonHandle getCollections(MarklogicConfig.Database database, int offset, int limit, String optionName) {
		QueryManager queryManager = marklogicConfig.newQueryManager(database);
		queryManager.setPageLength(limit);

		ValuesDefinition valuesDefinition = queryManager.newValuesDefinition("optCollections", optionName);

		return queryManager.values(valuesDefinition, new JacksonHandle(), offset);
	}
}