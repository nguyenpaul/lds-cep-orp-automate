package lds.cep.org.automate.boot.ml;

import com.fasterxml.jackson.databind.JsonNode;
import com.marklogic.client.query.StructuredQueryBuilder;
import com.marklogic.client.query.StructuredQueryDefinition;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.StreamSupport;

public class SearchQueryBuilder {

	private static final String VALUE = "value";
	private static final String CONSTRAINT_TYPE = "constraintType";
	private static final String CONSTRAINT = "constraint";
	final StructuredQueryBuilder queryBuilder;
	String criteria;

	public SearchQueryBuilder() {
		this.queryBuilder = new StructuredQueryBuilder();
	}

	public SearchQueryBuilder(String searchOption) {
		super();
		if (searchOption != null) {
			this.queryBuilder = new StructuredQueryBuilder(searchOption);
		} else {
			this.queryBuilder = new StructuredQueryBuilder();
		}

	}

	public StructuredQueryDefinition buildQueryWithCriteria(JsonNode filters) {
		QueryBuilderAndCriteria builder = new QueryBuilderAndCriteria(this.queryBuilder);
		StructuredQueryDefinition query = buildQuery(builder, filters);
		if (query != null && builder.criteria != null) {
			query.setCriteria(builder.criteria);
		}
		return query;
	}

	private StructuredQueryDefinition buildQuery(QueryBuilderAndCriteria builder, JsonNode filters) {
		if (filters.has("and")) {
			JsonNode and = filters.get("and");
			return builder.queryBuilder.and(arrayFromNode(builder, and));
		} else if (filters.has("or")) {
			JsonNode or = filters.get("or");
			return builder.queryBuilder.or(arrayFromNode(builder, or));
		} else if (filters.has("not")) {
			JsonNode not = filters.get("not");
			return builder.queryBuilder.not(buildQuery(builder, not));
		} else if (filters.has("near")) {
			JsonNode near = filters.get("near");
			return builder.queryBuilder.near(arrayFromNode(builder, near));
		} else {
			String type = filters.has("type") ? filters.get("type").asText() : "selection";
			if (type.equals("queryText")) {
				if (filters.has(CONSTRAINT)) {
					return createConstraint(builder, filters.get(CONSTRAINT_TYPE).asText(),
							filters.get(CONSTRAINT).asText(), "EQ", filters.get(VALUE));
				}

				String value = filters.get(VALUE).asText();
				if (value != null && !value.isEmpty()) {
					builder.criteria = value;
				}

				return builder.queryBuilder.and();
			} else {
				List<StructuredQueryDefinition> queries = new ArrayList<>();
				if (type.equals("selection")) {
					JsonNode value = filters.get(VALUE);
					if (value.isArray()) {
						for (JsonNode v : value) {
							queries.addAll(createSelectionQueries(builder, filters.get(CONSTRAINT_TYPE).asText(),
									filters.get(CONSTRAINT).asText(), v));
						}
					} else {
						queries.addAll(createSelectionQueries(builder, filters.get(CONSTRAINT_TYPE).asText(),
								filters.get(CONSTRAINT).asText(), value));
					}
				} else if (type.equals("range")) {
					filters.get(VALUE).fieldNames().forEachRemaining(key -> {
						String constraintType = filters.has(CONSTRAINT_TYPE) ? filters.get(CONSTRAINT_TYPE).asText()
								: "";
						queries.add(createConstraint(builder, constraintType, filters.get(CONSTRAINT).asText(),
								key.toUpperCase(), filters.get(VALUE).get(key)));
					});
				}

				if (queries.size() == 1) {
					return queries.get(0);
				}

				String filterMode = filters.has("mode") ? filters.get("mode").asText() : "and";
				if (filterMode.equals("or")) {
					return builder.queryBuilder.or(queries.toArray(new StructuredQueryDefinition[0]));
				}

				return builder.queryBuilder.and(queries.toArray(new StructuredQueryDefinition[0]));
			}
		}
	}

	private List<StructuredQueryDefinition> createSelectionQueries(QueryBuilderAndCriteria builder,
																   String constraintType, String constraint, JsonNode v) {
		List<StructuredQueryDefinition> queries = new ArrayList<>();
		if (v.has("not")) {
			queries.add(builder.queryBuilder
					.not(createConstraint(builder, constraintType, constraint, "EQ", v.get("not"))));
		} else {
			queries.add(createConstraint(builder, constraintType, constraint, "EQ", v));
		}
		return queries;
	}

	private StructuredQueryDefinition createConstraint(QueryBuilderAndCriteria builder, String constraintType,
													   String constraint, String operator, JsonNode value) {
		switch (constraintType) {
			case VALUE:
				return builder.queryBuilder.valueConstraint(constraint, value.asText());
			case "word":
				return builder.queryBuilder.wordConstraint(constraint, value.asText());
			case "custom":
				return builder.queryBuilder.customConstraint(constraint, value.asText());
			case "collection":
				return builder.queryBuilder.collectionConstraint(constraint, value.asText());
			default:
				return builder.queryBuilder.rangeConstraint(constraint, StructuredQueryBuilder.Operator.valueOf(operator),
						value.asText());
		}
	}

	private StructuredQueryDefinition[] arrayFromNode(QueryBuilderAndCriteria builder, JsonNode node) {
		StructuredQueryDefinition[] q;
		if (!node.isArray()) {
			q = new StructuredQueryDefinition[]{buildQuery(builder, node)};
		} else {
			q = StreamSupport.stream(node.spliterator(), false).map(jsonNode -> buildQuery(builder, jsonNode))
					.toArray(StructuredQueryDefinition[]::new);
		}
		return q;
	}


}

class QueryBuilderAndCriteria {

	final StructuredQueryBuilder queryBuilder;
	String criteria;

	public QueryBuilderAndCriteria(StructuredQueryBuilder queryBuilder) {
		this.queryBuilder = queryBuilder;
	}
}