package lds.cep.org.automate.boot.search;

public class And {
	String constraint;
	String constraintType;
	String mode;
	String type = "queryText";

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}


	public String getConstraint() {
		return constraint;
	}

	public void setConstraint(String constraint) {
		this.constraint = constraint;
	}

	public String getConstraintType() {
		return constraintType;
	}

	public void setConstraintType(String constraintType) {
		this.constraintType = constraintType;
	}

	public String getMode() {
		return mode;
	}

	public void setMode(String mode) {
		this.mode = mode;
	}

}
