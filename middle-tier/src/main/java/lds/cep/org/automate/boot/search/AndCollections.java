package lds.cep.org.automate.boot.search;

import java.util.List;

public class AndCollections extends And {
	List<String> value;

	public void setValue(List<String> value) {
		this.value = value;
	}

	public List<String> getValue() {
		return value;
	}
}
