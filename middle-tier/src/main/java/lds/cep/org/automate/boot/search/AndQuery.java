package lds.cep.org.automate.boot.search;

public class AndQuery extends And {
	String value;

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}
}
