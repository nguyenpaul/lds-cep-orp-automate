package lds.cep.org.automate.boot.search;

import java.util.List;

public class Filters {
	List<And> and;

	public List<And> getAnd() {
		return and;
	}

	public void setAnd(List<And> and) {
		this.and = and;
	}
}
