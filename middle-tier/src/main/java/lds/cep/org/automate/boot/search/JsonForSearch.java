package lds.cep.org.automate.boot.search;

public class JsonForSearch {

	Filters filters;
	Options options;

	public Filters getFilters() {
		return filters;
	}

	public void setFilters(Filters filters) {
		this.filters = filters;
	}

	public Options getOptions() {
		return options;
	}

	public void setOptions(Options options) {
		this.options = options;
	}
}
