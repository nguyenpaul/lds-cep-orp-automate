package lds.cep.org.automate.boot.service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.marklogic.client.document.DocumentManager.Metadata;
import com.marklogic.client.io.JacksonHandle;
import com.marklogic.client.io.StringHandle;
import lds.cep.org.automate.boot.ml.DocumentOption;
import lds.cep.org.automate.boot.ml.MarklogicConfig;
import lds.cep.org.automate.boot.ml.MarklogicService;
import lds.cep.org.automate.boot.util.SearchUtil;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class AdminOperationsService {

	private final MarklogicService mlService;

	public AdminOperationsService(MarklogicService mlService) {
		this.mlService = mlService;
	}

	public Boolean reloadDirectory(String path, MarklogicConfig.Database source, MarklogicConfig.Database dest) {
		mlService.deleteAllDocuments(path, dest);
		return mlService.copyAllDocuments(path, source, dest);
	}

	public StringHandle readJobsProperties(String path) {
		return mlService.readProperties(path, MarklogicConfig.Database.JOBS, Metadata.PROPERTIES);
	}

	public StringHandle readXML(String path) {
		return this.mlService.readXML(path, MarklogicConfig.Database.JOBS, DocumentOption.FULL);
	}

	public StringHandle readXMLHeMetadata(String uri) {
		return this.mlService.readXML(uri, MarklogicConfig.Database.JOBS, DocumentOption.METADATA);
	}

	public JsonNode searchByJsonNode(String query) throws JsonProcessingException {
		ObjectMapper mapper = new ObjectMapper();
		JsonNode jsonQuery = mapper.readTree(query);

		return SearchUtil.processResults(mlService.searchByJsonNode(jsonQuery, MarklogicConfig.Database.JOBS, "jobs"));
	}

	public JsonNode searchByString(String query, int offset, int limit, List<String> collections)
			throws JsonProcessingException {
		return mlService.searchByString(query, limit, offset, MarklogicConfig.Database.JOBS, "jobs", collections);
	}

	public JacksonHandle getCollections(int offset, int limit) {
		return mlService.getCollections(MarklogicConfig.Database.JOBS, offset, limit, "jobs");
	}

}
