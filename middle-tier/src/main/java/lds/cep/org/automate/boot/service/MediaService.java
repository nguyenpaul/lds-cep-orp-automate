/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lds.cep.org.automate.boot.service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.marklogic.client.document.DocumentManager.Metadata;
import com.marklogic.client.io.DocumentMetadataHandle;
import com.marklogic.client.io.DocumentMetadataHandle.DocumentCollections;
import com.marklogic.client.io.InputStreamHandle;
import com.marklogic.client.io.JacksonHandle;
import com.marklogic.client.io.StringHandle;
import lds.cep.org.automate.boot.ml.MarklogicConfig;
import lds.cep.org.automate.boot.ml.MarklogicService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.MimeTypeUtils;
import org.springframework.web.HttpMediaTypeNotSupportedException;

import javax.xml.namespace.QName;
import java.util.List;

/**
 * @author pnguyen
 */

@Service
public class MediaService {

	private final MarklogicService mlService;
	private final String[] authorizedType = {"application/pdf", "image/eps", "image/png", "image/tiff", "image/jpeg",
			"audio/mpeg", "video/mp4"};

	@Autowired
	public MediaService(MarklogicService mlService) {
		this.mlService = mlService;
	}

	public byte[] getBinary(String uri) {
		return mlService.getBinary(uri, MarklogicConfig.Database.MEDIA);
	}

	private boolean isTypeAuthorized(String mimeType) {
		for (String type : authorizedType) {
			if (mimeType.equals(type))
				return true;
		}
		return false;
	}

	public JacksonHandle getCollections(int offset, int limit) {
		return mlService.getCollections(MarklogicConfig.Database.MEDIA, offset, limit, "media");
	}

	public String saveBinary(String resourceId, String mimeType, InputStreamHandle content)
			throws HttpMediaTypeNotSupportedException {
		if (!isTypeAuthorized(mimeType)) {
			throw new HttpMediaTypeNotSupportedException(String.format("Content type %s is not supported", mimeType));
		}
		String file = resourceId.indexOf('.')>=0 ? resourceId. substring(0,resourceId.lastIndexOf('.')) : resourceId;
		String fileExtension = MimeTypeUtils.parseMimeType(mimeType).getSubtype();
		String uriMedia = "/LS/MEDIA/" + file + "." + fileExtension;

		DocumentMetadataHandle metadataHandle = new DocumentMetadataHandle();
		DocumentCollections collections = metadataHandle.getCollections();
		collections.add(mimeType);
		mlService.saveBinary(uriMedia, MarklogicConfig.Database.MEDIA, mimeType, content);
		mlService.addProperty(uriMedia, MarklogicConfig.Database.MEDIA, new QName(MarklogicConfig.NAMESPACE_HE, "id"), resourceId);
		return uriMedia;
	}


	public StringHandle getMetadataProperties(String uri) {
		return mlService.readProperties(uri, MarklogicConfig.Database.MEDIA, Metadata.PROPERTIES);
	}

	public JsonNode searchByJsonNode(String body) throws JsonProcessingException {
		ObjectMapper mapper = new ObjectMapper();
		JsonNode jsonQuery = mapper.readTree(body);
		return mlService.searchByJsonNode(jsonQuery, MarklogicConfig.Database.MEDIA, "media");
	}

	public JsonNode searchByString(String query, Integer start, Integer length, List<String> collections) throws JsonProcessingException {
		return mlService.searchByString(query, start, length, MarklogicConfig.Database.MEDIA, "media", collections);
	}
}
