/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lds.cep.org.automate.boot.service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.marklogic.client.document.DocumentManager.Metadata;
import com.marklogic.client.io.JacksonHandle;
import com.marklogic.client.io.StringHandle;
import lds.cep.org.automate.boot.ml.DocumentOption;
import lds.cep.org.automate.boot.ml.MarklogicConfig;
import lds.cep.org.automate.boot.ml.MarklogicService;
import lds.cep.org.automate.boot.util.SearchUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.OffsetDateTime;
import java.util.List;

/**
 * @author pnguyen
 */
@Service
public class OutputService {

	private final MarklogicService mlService;

	@Autowired
	public OutputService(MarklogicService mlService) {
		this.mlService = mlService;
	}

	public byte[] getContentDocument(String uri) {
		if (uri != null) {
			return mlService.getContentDocument(uri, MarklogicConfig.Database.FINAL);
		}
		return "".getBytes();
	}

	public StringHandle readXML(String uri) {
		return this.mlService.readXML(uri, MarklogicConfig.Database.FINAL, DocumentOption.FULL);
	}

	public JacksonHandle getCollections(int offset, int limit) {
		return mlService.getCollections(MarklogicConfig.Database.FINAL, offset, limit, "final");
	}

	public StringHandle getMetadataHe(String uri) {
		return this.mlService.readXML(uri, MarklogicConfig.Database.FINAL, DocumentOption.METADATA);
	}

	public StringHandle getMetadataDocument(String uri) {
		return mlService.readProperties(uri, MarklogicConfig.Database.FINAL, Metadata.METADATAVALUES);
	}

	public StringHandle getJsonContentDocument(String uri) {
		return mlService.readJSON(uri, MarklogicConfig.Database.FINAL);
	}

	public StringHandle getNitfXmlContentDocument(String uri) {
		return mlService.readXML(uri, MarklogicConfig.Database.FINAL, "identity-nitf");
	}

	public JsonNode searchByJsonNode(String query, String perimeter) throws JsonProcessingException {
		ObjectMapper mapper = new ObjectMapper();
		JsonNode jsonQuery = mapper.readTree(query);
		return SearchUtil.processResults(mlService.searchByJsonNode(jsonQuery, MarklogicConfig.Database.FINAL, perimeter));
	}

	public JsonNode searchByString(String query, Integer start, Integer length, String perimeter, List<String> collections)
			throws JsonProcessingException {
		return mlService.searchByString(query, start, length, MarklogicConfig.Database.FINAL, perimeter, collections);
	}

	public StringHandle searchUpdatedDocuments(OffsetDateTime datetimeStart, OffsetDateTime datetimeEnd,
											   List<String> collections, String directoryURI, Integer start, Integer length) {
		return mlService.searchCustom(MarklogicConfig.Database.FINAL, datetimeStart, datetimeEnd, collections, directoryURI, start,
				length);
	}

}