package lds.cep.org.automate.boot.service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.marklogic.client.io.InputStreamHandle;
import com.marklogic.client.io.JacksonHandle;
import com.marklogic.client.io.StringHandle;
import lds.cep.org.automate.boot.ml.DocumentOption;
import lds.cep.org.automate.boot.ml.MarklogicConfig;
import lds.cep.org.automate.boot.ml.MarklogicService;
import lds.cep.org.automate.boot.util.SearchUtil;
import lds.cep.org.automate.boot.util.Utils;
import lds.cep.org.automate.boot.util.XmlUtil;
import lds.cep.org.automate.boot.input.InputConfig;
import lds.cep.org.automate.boot.input.InputLine;
import lds.cep.org.automate.xml.validation.XmlValidationException;
import lds.cep.org.automate.model.DocType;
import lds.cep.org.automate.model.DocumentState;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.nio.file.Files;
import java.time.OffsetDateTime;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class ProvisioningService {

	private final MarklogicService mlService;
	private final InputConfig inputConfig;

	private final static Logger logger = LoggerFactory.getLogger(ProvisioningService.class);

	@Autowired
	public ProvisioningService(MarklogicService mlService, InputConfig inputConfig) {
		this.mlService = mlService;
		this.inputConfig = inputConfig;
	}

	public void validateXml(byte[] doc, lds.cep.org.automate.model.DocType docType) throws XmlValidationException, IOException {
		List<String> models = inputConfig.getInputLine(docType).getModels();
		if (models != null)
			XmlUtil.validate(models, doc);
	}

	public String postToHe(byte[] doc, String id, lds.cep.org.automate.model.DocType docType, String username, lds.cep.org.automate.model.DocumentState state) {

		InputLine inputLine = inputConfig.getInputLine(docType);
		InputStreamHandle inputStreamHandle = new InputStreamHandle();
		inputStreamHandle.fromBuffer(XmlUtil.addEnvelop(doc, id, username, docType, inputLine.getPublisher(), state));

		String uri = inputLine.getStagingUri() + id + ".xml";
		mlService.saveXML(uri, MarklogicConfig.Database.STAGING, inputStreamHandle, inputLine.getCollections());
		List<String> jobsUris = inputLine.getJobsUris().stream().map(str -> str + id + ".xml")
				.collect(Collectors.toList());
		mlService.saveMultipleXML(jobsUris, MarklogicConfig.Database.JOBS, inputStreamHandle, inputLine.getCollections());
		return uri;
	}

	public String postDirToHe(String dirPath, lds.cep.org.automate.model.DocType docType, String username) throws IOException {
		InputLine inputLine = inputConfig.getInputLine(docType);
		File dir = new File(dirPath);
		File[] files = dir.listFiles();

		logger.info("files = {}", (Object) files);

		assert files != null;
		for (File file : files) {
			String id = Utils.getBaseName(file.getName());
			byte[] content = Files.readAllBytes(file.toPath());
			postToHe(content, id, docType, username, DocumentState.DRAFT);
		}
		return inputLine.getStagingUri();
	}

	public String loadSamples(DocType type, String username) throws IOException {
		URL url = this.getClass().getClassLoader().getResource("sample/" + type.toString());
		assert url != null;
		return postDirToHe(url.getPath(), type, username);
	}

	public JsonNode searchByJsonNode(String query) throws JsonProcessingException {
		ObjectMapper mapper = new ObjectMapper();
		JsonNode jsonQuery = mapper.readTree(query);
		return SearchUtil.processResults(mlService.searchByJsonNode(jsonQuery, MarklogicConfig.Database.STAGING, "staging"));
	}

	public JsonNode searchByString(String query, Integer start, Integer length, List<String> collections) throws JsonProcessingException {
		return mlService.searchByString(query, start, length, MarklogicConfig.Database.STAGING, "staging", collections);
	}

	public JacksonHandle getCollections(int offset, int limit) {
		return mlService.getCollections(MarklogicConfig.Database.STAGING, offset, limit, "staging");
	}

	public StringHandle readXML(String uri) {
		return this.mlService.readXML(uri, MarklogicConfig.Database.STAGING, DocumentOption.FULL);
	}

	public StringHandle readXMLContent(String uri) {
		return this.mlService.readXML(uri, MarklogicConfig.Database.STAGING, DocumentOption.CONTENT);
	}

	public StringHandle readXMLHeMetadata(String uri) {
		return this.mlService.readXML(uri, MarklogicConfig.Database.STAGING, DocumentOption.METADATA);
	}

	public StringHandle searchUpdatedDocuments(OffsetDateTime datetimeStart, OffsetDateTime datetimeEnd,
											   List<String> collections, String directoryURI, Integer start, Integer length) {
		return mlService.searchCustom(MarklogicConfig.Database.STAGING, datetimeStart, datetimeEnd, collections, directoryURI, start,
				length);
	}
}
