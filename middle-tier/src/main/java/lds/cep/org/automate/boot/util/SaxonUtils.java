package lds.cep.org.automate.boot.util;

import net.sf.saxon.s9api.*;
import net.sf.saxon.serialize.MessageEmitter;
import net.sf.saxon.trans.XPathException;
import net.sf.saxon.value.CalendarValue;
import net.sf.saxon.value.DateTimeValue;
import org.xml.sax.InputSource;

import javax.xml.transform.URIResolver;
import javax.xml.transform.sax.SAXSource;
import java.io.*;
import java.net.URI;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

public class SaxonUtils {
	private static final Processor P = new Processor(true);

	public static DateTimeValue dateToSaxonDateTime(LocalDateTime localDateTime) {
		return new DateTimeValue(localDateTime.getYear(),
				(byte) localDateTime.getMonth().getValue(),
				(byte) localDateTime.getDayOfMonth(),
				(byte) localDateTime.getHour(),
				(byte) localDateTime.getMinute(),
				(byte) localDateTime.getSecond(),
				0,
				CalendarValue.NO_TIMEZONE,
				false
		);
	}

	public static XdmNode transform(XdmNode node, Map<URL, List<XsltParam>> xslts)
			throws SaxonApiException, IOException {
		return transform(node, xslts, null);
	}

	public static XdmNode transform(XdmNode node, URL xslt)
			throws SaxonApiException, IOException {
		Map<URL, List<XsltParam>> xslts = new LinkedHashMap<>();
		xslts.put(xslt, new ArrayList<>());
		return transform(node, xslts, null);
	}

	public static XdmNode transform(XdmNode node, Map<URL, List<XsltParam>> xslts,
									URIResolver resolver) throws SaxonApiException, IOException {
		XsltCompiler xsltC = P.newXsltCompiler();
		xsltC.setURIResolver(resolver);
		for (Entry<URL, List<XsltParam>> e : xslts.entrySet()) {
			URL s = e.getKey();
			InputSource xsltIs = new InputSource(s.openStream());
			XsltExecutable exe = xsltC.compile(new SAXSource(xsltIs));
			XsltTransformer load = exe.load();
			for (XsltParam p : e.getValue()) {
				load.setParameter(new QName(p.getKey()), p.getValue());
			}
			XdmDestination dest = new XdmDestination();
			load.setDestination(dest);
			load.setInitialContextNode(node);
			load.transform();
			node = dest.getXdmNode();
		}
		return node;
	}

	public static XdmNode transform(XdmNode node, XsltExecutable exe, List<XsltParam> params) throws SaxonApiException {
		XsltTransformer load = exe.load();
		load.setInitialContextNode(node);
		return applyXslt(params, load);
	}

	private static XdmNode applyXslt(List<XsltParam> params, XsltTransformer load) throws SaxonApiException {
		XdmDestination dest = new XdmDestination();
		load.setDestination(dest);
		for (XsltParam p : params) {
			load.setParameter(new QName(p.getKey()), p.getValue());
		}
		final StringWriter messageOut = new StringWriter();
		load.getUnderlyingController().setMessageEmitter(new MessageEmitter() {
			@Override
			public void open() throws XPathException {
				setWriter(messageOut);
				super.open();
			}
		});
		try {
			load.transform();
		} catch (SaxonApiException e) {
			String message = messageOut.toString();
			throw new SaxonApiException(message);
		}
		return dest.getXdmNode();
	}

	public static XdmNode nodeFromFile(File f) {
		try {
			DocumentBuilder builder = P.newDocumentBuilder();
			return builder.build(f);
		} catch (SaxonApiException e) {
			throw new RuntimeException(e);
		}
	}

	public static void write(XdmNode n, XdmDestination d)
			throws SaxonApiException {
		P.writeXdmValue(n, d);
		d.close();
	}

	public static XdmNode nodeFromReader(Reader in) {
		try {
			DocumentBuilder builder = P.newDocumentBuilder();
			InputSource ins = new InputSource(in);
			return builder.build(new SAXSource(ins));
		} catch (SaxonApiException e) {
			throw new RuntimeException(e);
		}
	}

	public static XdmNode nodeFromStream(InputStream in) {
		try {
			DocumentBuilder builder = P.newDocumentBuilder();
			InputSource ins = new InputSource(in);
			return builder.build(new SAXSource(ins));
		} catch (SaxonApiException e) {
			throw new RuntimeException(e);
		}
	}

	public static XdmNode nodeFromReader(Reader in, URI uri) throws SaxonApiException {
		DocumentBuilder builder = P.newDocumentBuilder();
		builder.setBaseURI(uri);
		InputSource ins = new InputSource(in);
		return builder.build(new SAXSource(ins));
	}

	public static XdmValue Xpath(XdmNode n, String xpath) {
		try {
			XPathCompiler comp = P.newXPathCompiler();
			XPathExecutable compile = comp.compile(xpath);
			XPathSelector load = compile.load();
			load.setContextItem(n);
			return load.evaluate();
		} catch (SaxonApiException e) {
			throw new RuntimeException(e);
		}
	}

	public static void serialize(XdmNode n, OutputStream os, boolean indent) {
		Serializer ser = P.newSerializer(os);
		ser.setOutputProperty(Serializer.Property.INDENT, indent ? "yes" : "no");
		try {
			ser.serializeNode(n);
		} catch (SaxonApiException e) {
			throw new RuntimeException(e);
		}
	}

	public static String serialize(XdmNode n) throws SaxonApiException {
		Serializer ser = P.newSerializer();
		ser.setOutputProperty(Serializer.Property.OMIT_XML_DECLARATION, "no");
		//ser.setOutputProperty(Serializer.Property.INDENT, "yes");
		return ser.serializeNodeToString(n);
	}

	public static String toString(XdmNode input) {
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		Serializer ser = P.newSerializer(baos);
		ser.setOutputProperty(Serializer.Property.INDENT, "no");
		try {
			ser.serializeNode(input);
			return baos.toString(StandardCharsets.UTF_8);
		} catch (SaxonApiException e) {
			throw new RuntimeException(e);
		}
	}

	public static XsltExecutable getXslExe(InputStream f) throws SaxonApiException {
		XsltCompiler xsltC = P.newXsltCompiler();
		InputSource is = new InputSource(f);
		return xsltC.compile(new SAXSource(is));
	}
}
