package lds.cep.org.automate.boot.util;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ObjectNode;

import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

public class SearchUtil {

	static final org.slf4j.Logger logger = org.slf4j.LoggerFactory.getLogger("SearchUtil");

	private SearchUtil() {
		throw new IllegalStateException("Utility class");
	}

	public static JsonNode processResults(JsonNode node) {
		StreamSupport.stream(node.get("results").spliterator(), false)
				.map(jsonNode -> (ObjectNode) jsonNode)
				.map(jsonNode -> {
					jsonNode.put("id", URLEncoder.encode(jsonNode.get("uri").asText(), StandardCharsets.UTF_8));
					return jsonNode;
				}).collect(Collectors.toList());
		return node;
	}
}
