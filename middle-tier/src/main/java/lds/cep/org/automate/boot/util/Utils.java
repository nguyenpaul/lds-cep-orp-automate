package lds.cep.org.automate.boot.util;

import lds.cep.org.automate.model.Error;
import lds.cep.org.automate.model.NestedError;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.time.OffsetDateTime;
import java.util.ArrayList;
import java.util.List;

public class Utils {

	private Utils() {
		throw new IllegalStateException("Utility class");
	}

	// with JAVA 9 :
	// https://stackoverflow.com/questions/43157/easy-way-to-write-contents-of-a-java-inputstream-to-an-outputstream/39440936#39440936
	public static void copy(InputStream in, OutputStream out) throws IOException {
		byte[] buf = new byte[1024];
		int len = in.read(buf);
		while (len != -1) {
			out.write(buf, 0, len);
			len = in.read(buf);
		}
	}

	public static String getBaseName(String fileName) {
		int index = fileName.lastIndexOf('.');
		if (index == -1) {
			return fileName;
		} else {
			return fileName.substring(0, index);
		}
	}

	public static Error createError(Integer status, String code, String message, NestedError nestedError) {
		List<NestedError> arrayNestedError = new ArrayList<>();
		arrayNestedError.add(nestedError);
		return createError(status, code, message, arrayNestedError);
	}

	public static Error createError(Integer status, String code, String message) {
		return createError(status, code, message, new ArrayList<>());
	}

	public static Error createError(Integer status, String code, String message, List<NestedError> nestedError) {
		Error error = new Error();
		error.setStatus(status);
		error.setCode(code);
		error.setMessage(message);
		error.setTimestamp(OffsetDateTime.now());
		error.errors(nestedError);
		return error;
	}

	public static NestedError createNestedError(String code, String field, String message) {
		NestedError nestedError = new NestedError();
		nestedError.setCode(code);
		nestedError.setField(field);
		nestedError.setMessage(message);
		return nestedError;
	}
}
