package lds.cep.org.automate.boot.util;

import lds.cep.org.automate.boot.exception.AppException;
import lds.cep.org.automate.boot.exception.UserException;
import lds.cep.org.automate.model.DocType;
import lds.cep.org.automate.model.DocumentState;
import lds.cep.org.automate.xml.validation.XmlValidationException;
import lds.cep.org.automate.xml.validation.XmlValidator;
import net.sf.saxon.s9api.SaxonApiException;
import net.sf.saxon.s9api.XdmAtomicValue;
import net.sf.saxon.s9api.XdmNode;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.xml.sax.InputSource;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.util.*;

public class XmlUtil {

	private static final Logger logger = LoggerFactory.getLogger(XmlUtil.class);
	private static final ClassLoader CL = XmlUtil.class.getClassLoader();
	private static final String ADD_ENVELOP_XSL = "addEnvelope.xsl";
	private static final String CATALOG_LOCAL = "catalog-local.xml";

	private XmlUtil() {
		throw new IllegalStateException("Utility class");
	}

	static {
		System.setProperty("java.protocol.handler.pkgs", "eu.lefebvresarrut.he.boot");
	}

	public static void validate(List<String> models, byte[] xml) throws IOException, XmlValidationException {
		List<String> catalogsPath = Collections.singletonList(CATALOG_LOCAL);
		// validate the document with each model
		// the validator throws an exception if the document is not valid or not well
		// formed
		for (String modelUri : models) {
			try (ByteArrayInputStream bain = new ByteArrayInputStream(xml)) {
				logger.debug("validate input stream with '{}'", modelUri);
				XmlValidator.validate(new InputSource(bain), modelUri, catalogsPath);
			}
		}
	}

	public static void heConformity(DocType type, byte[] doc) {
	}

	public static byte[] addEnvelop(byte[] doc, String id, String username, DocType docType, String publisher, DocumentState state) {
		try (ByteArrayInputStream bain = new ByteArrayInputStream(doc)) {
			XdmNode node = SaxonUtils.nodeFromStream(bain);
			List<XsltParam> params = new ArrayList<>();
			params.add(new XsltParam(XdmAtomicValue.makeAtomicValue(id), "id"));
			params.add(new XsltParam(XdmAtomicValue.makeAtomicValue(state.toString()), "version"));
			params.add(new XsltParam(XdmAtomicValue.makeAtomicValue(username), "import-endpoint-user"));
			// TODO add relevant publisher
			params.add(new XsltParam(XdmAtomicValue.makeAtomicValue(publisher), "publisher"));
			params.add(new XsltParam(XdmAtomicValue.makeAtomicValue(docType.toString()), "type"));
			// TODO add relevant source name
			params.add(new XsltParam(XdmAtomicValue.makeAtomicValue("source"), "import-source-name"));
			URL xslt = CL.getResource(ADD_ENVELOP_XSL);
			Map<URL, List<XsltParam>> xslts = new HashMap<>();
			xslts.put(xslt, params);
			XdmNode result = SaxonUtils.transform(node, xslts);
			// logger.info("result: " + result);
			return result.toString().getBytes(StandardCharsets.UTF_8);

		} catch (IOException e) {
			throw new AppException("Problem while adding envelop", e);
		} catch (SaxonApiException e) {
			throw new UserException("XML provided cannot be transformed", e);
		}
	}

}
