package lds.cep.org.automate.boot.util;

import net.sf.saxon.s9api.XdmValue;

public class XsltParam {
	private final XdmValue value;
	private final String key;

	public XsltParam(XdmValue value, String key) {
		this.value = value;
		this.key = key;
	}

	public String getKey() {
		return key;
	}

	public XdmValue getValue() {
		return value;
	}
}
