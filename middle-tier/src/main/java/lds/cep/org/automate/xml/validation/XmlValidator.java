package lds.cep.org.automate.xml.validation;

import com.thaiopensource.resolver.Resolver;
import com.thaiopensource.resolver.catalog.CatalogResolver;
import com.thaiopensource.util.PropertyMap;
import com.thaiopensource.util.PropertyMapBuilder;
import com.thaiopensource.validate.ValidateProperty;
import com.thaiopensource.validate.ValidationDriver;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class XmlValidator {

	private static final ClassLoader CL = XmlValidator.class.getClassLoader();

	private XmlValidator() {
		throw new IllegalStateException("Utility class");
	}

	// Declare handler for cp protocol
	// cp protocol is needed to resolve schema import within a schema included as
	// maven dependency
	static {
		System.setProperty("java.protocol.handler.pkgs", "eu.lefebvresarrut.he.xml.validation");
	}

	/*
	 * Validate any XML file with RNG, RNC, Schematron1.5, Iso-Schematron, NVDL, but
	 * not with DTD
	 *
	 * @param xmlUri URI of the XML document, it might use cp protocol :
	 * cp:/path/to/xml.xml
	 *
	 * @param schemaURI URI of the schema, it might use cp protocol :
	 * cp:/path/to/model.rng
	 *
	 * @param catalogsPath list catalogs without protocol, to be loaded with
	 * classLoader (no cp protocol) Any relative URI or cp:/ URI will work within
	 * xmlURI/schemaUri but not within catalogsPath
	 */
	public static void validate(String xmlUri, String schemaUri, List<String> catalogsPath)
			throws XmlValidationException {
		validate(new InputSource(xmlUri), schemaUri, catalogsPath);
	}

	/*
	 * Validate any XML document with RNG, RNC, Schematron1.5, Iso-Schematron, NVDL,
	 * but not with DTD
	 *
	 * @param xml an XML document wrapped as an InputSource
	 *
	 * @param schemaUri URI of the schema, it might use cp protocol :
	 * cp:/path/to/model.rng
	 *
	 * @param catalogsPath list of catalogs without protocol, to be loaded with
	 * classLoader (no cp protocol) Any relative URI or cp:/ URI will work within
	 * xmlURI/schemaUri but not within catalogsPath
	 */
	public static void validate(InputSource xml, String schemaUri, List<String> catalogsPath)
			throws XmlValidationException {
		_validate(xml, schemaUri, catalogsPath);
	}

	/*
	 * Validate any XML file with RNG, RNC, Schematron1.5, Iso-Schematron, NVDL, but
	 * not with DTD
	 *
	 * @param xmlURI is a string, it might use cp protocol : cp:/path/to/xml.xml
	 *
	 * @param schemaURI is a string, it might use cp protocol : cp:/path/to/xml.xml
	 *
	 * @param catalogsPath list of catalogs without protocol, to be loaded with
	 * classLoader (no cp protocol) Any relative URI or cp:/ URI will work within
	 * xml/schemaUri but not within xmlCatalogsPath
	 */
	private static void _validate(InputSource xml, String schemaUri, List<String> catalogsPath)
			throws XmlValidationException {
		XmlValidationErrorHandler errorHandler = new XmlValidationErrorHandler();
		ValidationDriver validationDriver = new ValidationDriver(createPropertyMap(catalogsPath, errorHandler));

		// load schema
		// simple check for DTD, but validation will also fail if a DTD is loaded from
		// the schema
		if (schemaUri.toLowerCase().endsWith("dtd")) {
			throw new XmlValidationException(
					String.format("Validating with a DTD is not implemented ('%s')", schemaUri));
		}
		try {
			validationDriver.loadSchema(new InputSource(schemaUri));
		} catch (IOException e) {
			throw new XmlValidationException(String.format("Error loading schema from URI '%s' : %s", schemaUri, e));
		} catch (SAXException e) {
			throw new XmlValidationException(String.format("Schema parse error for '%s' : %s", schemaUri, e));
		}
		// validate
		try {
			if (!validationDriver.validate(xml)) {
				throw new XmlValidationException(String.format("XML invalid against model '%s'", schemaUri),
						errorHandler.getReport());
			}
		} catch (IOException e) {
			throw new XmlValidationException(
					String.format("XML read error from InputSource '%s' : %s", xml.getSystemId(), e),
					errorHandler.getReport());
		} catch (SAXException e) {
			throw new XmlValidationException(String.format("XML parse error for '%s'", xml.getSystemId()),
					errorHandler.getReport());
		}
	}

	private static PropertyMap createPropertyMap(List<String> catalogsPath, XmlValidationErrorHandler errHandler)
			throws XmlValidationException {
		PropertyMapBuilder propertyMapBuilder = new PropertyMapBuilder();

		// Set PropertyMap RESOLVER with catalogs
		if (catalogsPath != null) {
			List<String> catalogs = new ArrayList<>();
			for (String catalogUri : catalogsPath) {
				try {
					catalogs.add(Objects.requireNonNull(CL.getResource(catalogUri)).toURI().toASCIIString());
				} catch (URISyntaxException e) {
					throw new XmlValidationException(String.format("Bad URI syntax for catalog '%s'", catalogUri),
							errHandler.getReport());
				}
			}
			Resolver catalogResolver = new CatalogResolver(catalogs);
			propertyMapBuilder.put(ValidateProperty.RESOLVER, catalogResolver);
		}

		propertyMapBuilder.put(ValidateProperty.ERROR_HANDLER, errHandler);

		return propertyMapBuilder.toPropertyMap();
	}
}
