<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
  xmlns:xs="http://www.w3.org/2001/XMLSchema"
  xmlns:xd="http://www.oxygenxml.com/ns/doc/xsl"
  xmlns:es="http://marklogic.com/entity-services"
  xmlns:he="http://www.lefebvre-sarrut.eu/ns/hubeditorial"
  xmlns:els="http://www.lefebvre-sarrut.eu/ns"
  xmlns:xf="http://www.lefebvre-sarrut.eu/ns/xmlfirst"
  xmlns="http://www.w3.org/1999/xhtml"
  exclude-result-prefixes="#all"
  version="3.0">
  
  <xd:doc scope="stylesheet">
    <xd:desc>
      <xd:p>Add an envelope to wrap the input document with he import metadata</xd:p>
    </xd:desc>
  </xd:doc>
  
  <xsl:param name="id" as="xs:string" required="true"/>
  <xsl:param name="version" as="xs:string" required="false"/>
  <xsl:param name="import-endpoint-user" as="xs:string" required="true"/>
  <xsl:param name="import-source-name" as="xs:string" required="true"/>
  <xsl:param name="publisher" as="xs:string" required="true"/>
  <xsl:param name="type" as="xs:string" required="true"/>
  
  <xsl:variable name="import-date" select="current-dateTime()"/>
  
  <xsl:template match="/">
    <es:envelope>
      <es:headers>
        <he:mimetype>application/xml</he:mimetype>
        <he:id><xsl:sequence select="$id"/></he:id>
        <he:version><xsl:sequence select="$version"/></he:version>
        <he:import-date><xsl:sequence select="$import-date"/></he:import-date>
        <he:import-endpoint-user><xsl:sequence select="$import-endpoint-user"/></he:import-endpoint-user>
        <he:import-source-name><xsl:sequence select="$import-source-name"/></he:import-source-name>
        <he:publisher><xsl:sequence select="$publisher"/></he:publisher>
        <he:type><xsl:sequence select="$type"/></he:type>
      </es:headers>
      <es:instance>
        <xsl:sequence select="."/>
      </es:instance>
    </es:envelope>
  </xsl:template>
  
</xsl:stylesheet>