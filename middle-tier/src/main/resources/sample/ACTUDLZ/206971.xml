<?xml version="1.0" encoding="UTF-8"?>
<DLZ_FLASH gratuit="true" alaune="true">
  <TYPE_ACTUALITE>FLASH</TYPE_ACTUALITE>
  <FL_ACT_ID>206971</FL_ACT_ID>
  <FL_TYPE_FLASH>ESS</FL_TYPE_FLASH>
  <FL_TITRE>Assurance chômage : suspension des nouvelles règles de calcul de l'allocation</FL_TITRE>
  <FL_AUTEUR>Caroline Dechristé</FL_AUTEUR>
  <FL_DATE/>
  <FL_DATE_REF2/>
  <FL_SOMMAIRE>
    <p xmlns="http://www.w3.org/1999/xhtml">Saisie par six organisations syndicales, une ordonnance des référés du Conseil d'État suspend les règles de calcul du montant de l'allocation chômage qui devaient entrer en vigueur le 1<sup>er</sup> juillet. « Les incertitudes sur la situation économique ne permettent pas de mettre en place, à cette date, ces nouvelles règles qui sont censées favoriser la stabilité de l'emploi en rendant moins favorable l'indemnisation du chômage des salariés ayant alterné contrats courts et inactivité. »</p>
  </FL_SOMMAIRE>
  <FL_OBS>
    <p xmlns="http://www.w3.org/1999/xhtml">Énième rebondissement dans la non-application de la réforme de l'assurance chômage, les nouvelles modalités de détermination du montant de l'allocation d'assurance chômage ne s'appliqueront pas au 1<sup>er</sup> juillet. Une ordonnance de référé du Conseil d'État a en effet décidé que « les incertitudes sur la situation économique ne permettent pas de mettre en place » au 1<sup>er</sup> juillet les nouvelles règles, « qui sont censées favoriser la stabilité de l'emploi en rendant moins favorable l'indemnisation du chômage des salariés ayant alterné contrats courts et inactivité. […] De nombreuses incertitudes subsistent quant à l'évolution de la crise sanitaire et ses conséquences économiques sur la situation de celles des entreprises qui recourent largement aux contrats courts pour répondre à des besoins temporaires », justifie la plus haute juridiction de l'ordre administratif. « Or ces nouvelles règles de calcul des allocations chômage pénaliseront de manière significative les salariés de ces secteurs, qui subissent plus qu'ils ne choisissent l'alternance entre périodes de travail et périodes d'inactivité. »</p>
    <p xmlns="http://www.w3.org/1999/xhtml">Les nouvelles règles de détermination des droits des salariés en matière d'assurance chômage résultent – après l'impossibilité pour les partenaires sociaux de trouver un accord satisfaisant les objectifs dictés par le gouvernement – d'un décret du 26 juillet 2019 qui devait remplacer les règles issues de la convention de 2017 au 1<sup>er</sup> novembre 2019. Application reportée dans un premier temps à l'aune de crise sanitaire. Puis plusieurs dispositions de la réforme de l'assurance chômage avaient été censurées par le Conseil d'État le 25 novembre 2020 et notamment les celles relatives aux nouvelles modalités de calcul du salaire journalier de référence (SJR). Le juge administratif y voyait certes un objectif légitime (contrer les effets pervers du recours à des contrats courts fractionné qui permettait pour un même nombre d'heures de travail d'avoir un SJR plus élevé que le salarié en CDI à temps partiel sur la même période), mais relevait dans le même temps que les nouvelles règles conduisaient à des variations du SJR allant du « simple au quadruple » pour un même nombre d'heures de travail. En pénalisant fortement les allocataires travaillant de manière discontinue, le nouveau mode de calcul apparaissait ainsi à l'appréciation du Conseil d'État opérer une différence de traitement manifestement disproportionnée au regard du motif d'intérêt général poursuivi (v. CE 25 nov. 2021, n° 434920, <a class="type_link_external_link" href="https://www.dalloz-actualite.fr/flash/actualites-assurance-chomage-censure-par-conseil-d-etat-et-prolongation-d-indemnisation#results_box" title="">Dalloz actualité, 1<sup>er</sup> déc. 2020, obs. L. Malfettes</a> ; AJDA 2020. 346 <a class="dz_hype" href="http://www.dalloz.fr/lien?famille=revues&amp;dochype=AJDA/JURIS/2020/2483" title="consulter le fond documentaire">
      <img class="node_body_image" height="13" src="https://www.dalloz-actualite.fr/sites/all/themes/dallozactu/icons/type_dalloz_fr_link.png" width="12"/>
    </a>).</p>
    <p xmlns="http://www.w3.org/1999/xhtml">En réponse et pour corriger les règles ayant fait l'objet de l'annulation, un décret du 30 mars 2021 ajoutait un mécanisme de plancher au calcul du salaire journalier de référence afin d'éviter qu'il puisse varier du « simple au quadruple » (v. décr. n° 2021-346). Mais, pour l'ensemble des syndicats, les inégalités de traitement entre demandeurs d'emploi ayant travaillé une même durée pour une même rémunération mais selon un rythme différent existaient toujours et étaient en contradiction avec le principe assurantiel du régime d'assurance chômage en prenant en compte des périodes non travaillées. Toutes les organisations syndicales représentatives – à l'exception de la CFTC – ont déposé un recours auprès du Conseil d'État dans l'objectif d'obtenir la suspension de l'application des mesures de détermination de l'allocation d'assurance chômage afin qu'elle n'entre pas en vigueur au 1<sup>er</sup> juillet et, dans le même temps, elles ont déposé un recours au fond pour en obtenir l'annulation du décret du 30 mars.</p>
    <p xmlns="http://www.w3.org/1999/xhtml">La suspension du décret sonne pour les syndicats comme une victoire. Mais le gouvernement temporise et la ministre du Travail, Élisabeth Borne, a d'ores et déjà précisé sur RTL que le ministère du Travail prendra en urgence un décret « pour que les règles actuelles puissent continuer à s'appliquer “au-delà du 1<sup>er</sup> juillet” ».</p>
    <p xmlns="http://www.w3.org/1999/xhtml">Sur le fond, le Conseil d'État doit se prononcer « dans les prochains mois » et son communiqué prend le soin de préciser que la décision de suspension ne remet « pas en cause le principe de la réforme ».</p>
    <p xmlns="http://www.w3.org/1999/xhtml">Le Conseil d'État devra ainsi se prononcer sur une violation au principe d'égalité avancée par les syndicats pour justifier une annulation de la réforme ou confirmer un changement de paradigme de l'assurance chômage qui n'aurait plus comme vocation la couverture assurantielle de la privation d'emploi. Comme l'explique Mathieu Grégoire, la réforme de l'assurance chômage introduite par les décrets du 26 juillet 2019 et 30 mars 2021 « n'est pas une réforme paramétrique. C'est une réforme systémique. Il ne s'agit pas de donner une nouvelle définition au “salaire journalier de référence”. Il s'agit d'introduire un nouvel objet qui n'est pas un salaire, mais un indicateur synthétique de salaire et de quantum d'emploi, en lieu et place de ce salaire journalier. […] Le changement d'objet témoigne du caractère systémique de la réforme qui éloigne profondément le dispositif d'une logique assurantielle de remplacement du salaire pour les salariés en situation de privation d'emploi » (v. M. Grégoire, Réforme de l'assurance chômage : vers la fin de la couverture assurantielle de la privation d'emploi, RDT 2021. 364, à paraître).</p>
  </FL_OBS>
  <FL_OBS_CARMAX>200</FL_OBS_CARMAX>
  <FL_REDACT_NOTE/>
  <DLZ_PARUTION_ACTU>
    <PAR_ACTU_DEB>23/06/2021</PAR_ACTU_DEB>
    <PAR_ACTU_FIN/>
  </DLZ_PARUTION_ACTU>
  <DLZ_FICHIERS>
    <DLZ_FICHIER>
      <FACT_LIB>CE, ord., 22 juin 2021, req. n° 452210</FACT_LIB>
      <FIC_LIB>452210_et_suivants.pdf</FIC_LIB>
      <TAILLE>512492</TAILLE>
      <URL>https://www.dalloz-actualite.fr/document/ce-ord-22-juin-2021-req-n-452210</URL>
    </DLZ_FICHIER>
  </DLZ_FICHIERS>
  <DLZ_IMGS>
    <DLZ_IMG>
      <IMG_LIB>fl_pinces_a_linge_corde_vue_nf.jpg</IMG_LIB>
      <TAILLE>248879</TAILLE>
      <URL>https://www.dalloz-actualite.fr/sites/dalloz-actualite.fr/files/images/2021/06/fl-pinces-linge-corde-vue-nf.jpg</URL>
    </DLZ_IMG>
  </DLZ_IMGS>
  <DLZ_MATIERES>
    <DLZ_MATIERE>
      <DLZ_M>Social</DLZ_M>
      <DLZ_THS>
        <DLZ_TH>Chômage et emploi</DLZ_TH>
      </DLZ_THS>
      <GTW-THS>
        <GTW-TH>Contrat de travail</GTW-TH>
      </GTW-THS>
    </DLZ_MATIERE>
  </DLZ_MATIERES>
</DLZ_FLASH>